apiVersion: apps/v1
kind: Deployment
metadata:
  name: sepa-payment-service
  labels:
    app: sepa-payment-service
spec:
  selector:
    matchLabels:
      app: sepa-payment-service
  replicas: {{ .Values.replicaCount }}
  template:
    metadata:
      labels:
          app: sepa-payment-service
    spec:
      containers:
      - name: sepa-payment-service-api
        image: {{ .Values.image.application.name }}
        imagePullPolicy: {{ .Values.image.application.pullPolicy }}
        volumeMounts:
        - name: spring-config-volume
          mountPath: /bootstrap.yaml
          subPath: bootstrap.yaml
        ports:
        - containerPort: 8080
        livenessProbe:
          httpGet:
            path: /actuator/health
            port: 8080
          initialDelaySeconds: 20
          periodSeconds: 5
        readinessProbe:
          httpGet:
            path: /actuator/health
            port: 8080
          initialDelaySeconds: 20
          periodSeconds: 5
      volumes:
      - name: spring-config-volume
        configMap:
          name: sepa-payment-service-springconfig
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: sepa-payment-service-apidoc
  labels:
    app: sepa-payment-service
spec:
  selector:
    matchLabels:
      app: sepa-payment-service-apidoc
  replicas: 1
  template:
    metadata:
      labels:
          app: sepa-payment-service-apidoc
    spec:
      containers:
      - name: sepa-payment-service-apidoc
        image: swaggerapi/swagger-ui
        imagePullPolicy: IfNotPresent
        env:
        - name: SWAGGER_JSON
          value: "/apidoc.yaml"
        volumeMounts:
        - name: apidoc-volume
          mountPath: /apidoc.yaml
          subPath: apidoc.yaml
        ports:
        - containerPort: 8080
        livenessProbe:
          httpGet:
            path: /
            port: 8080
          initialDelaySeconds: 5
          periodSeconds: 3
        readinessProbe:
          httpGet:
            path: /
            port: 8080
          initialDelaySeconds: 5
          periodSeconds: 3
      volumes:
      - name: apidoc-volume
        configMap:
          name: sepa-payment-service-apidoc
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: sepa-payment-service-config-server
  labels:
    app: sepa-payment-service
spec:
  selector:
    matchLabels:
      app: sepa-payment-service-config-server
  replicas: 1
  template:
    metadata:
      labels:
          app: sepa-payment-service-config-server
    spec:
      containers:
      - name: sepa-payment-service-config-server
        image: {{ .Values.image.configServer.name }}
        imagePullPolicy: {{ .Values.image.configServer.pullPolicy }}
        env:
        - name: SPRING_CLOUD_CONFIG_SERVER_GIT_URI
          value: {{ .Values.spec.env.springConfigServer.url }}
        ports:
        - containerPort: 8080
        livenessProbe:
          httpGet:
            path: /actuator/health
            port: 8080
          initialDelaySeconds: 10
          timeoutSeconds: 5
        readinessProbe:
          httpGet:
            path: /actuator/health
            port: 8080
          initialDelaySeconds: 10
          timeoutSeconds: 5
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: sepa-payment-service-db
  labels:
    app: sepa-payment-service
spec:
  selector:
    matchLabels:
      app: sepa-payment-service-db
  replicas: 1
  template:
    metadata:
      labels:
        app: sepa-payment-service-db
    spec:
      containers:
        - name: sepa-payment-service-db
          image: postgres
          imagePullPolicy: {{ .Values.image.configServer.pullPolicy }}
          ports:
          - containerPort: 5432
          env:
          - name: POSTGRES_PASSWORD
            value: test
