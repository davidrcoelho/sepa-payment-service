
### Create jobs

job to create document
0. List all tenants 
    0. For each tenant list all creditors
        0. For each brokerCreditor
            - Lock brokerCreditor (optimistic)
            
            *if there is NO **document** for the current day*
                - status: NEW
                - Create a **document** record for first payments the today()
                - Create a **document** record for recurring payments the today()
            
            *else:*
            
                - select the document for the current day
            - List all payments for this brokerCreditor 
                where requestedCollectionDate <= today() + scheduledDaysBeforeCollectionDate
                    and document == null
                0. For each payment 
                    - assign payment to the **document** record

job to send the document to sepa-broker
0. List all documents with status NEW
    0. for each document
        0. Lock document (optimistic)
            - generate an UUID to be used as requestId
            
            if document.sepaBrokerDocumentId is null
            
                - Create a document at sepa-broker
                - store sepa-broker id with the document
                
            0. List all payments with status CREATED and sepa-broker.id is null
                0. For each payment
                    - generate requestId and assign to it (use document.requestId?)
                    - send this payment to sepa-broker
                    - store sepa-broker id with the payment
        
        0. Set document.status = READY
        
job to send documents that are READY to payment
0. List all documents with status READY and jobId is null
    
    - get sepa-broker job
    
        if there is no job
            0. Create job at sepa-broker
            0. assign jobId
        
        else:
            0. assign jobId
    
## Implement validation to make sure the scenario below does not happen:
```
Create an account A1
Add a mandate M1 to A1
Add a payment P1 to M1 with requestedCollectionDate = 30/07/2020
Add an amendment M2 to mandate M1
Add a payment P2 to M2 with requestedCollectionDate = 29/07/2020 (before the last payment)
    - amendment in this scenario will refer to M1 which was not sent do the bank yet
```