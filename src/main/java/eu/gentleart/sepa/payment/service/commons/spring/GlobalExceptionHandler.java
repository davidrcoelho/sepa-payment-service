package eu.gentleart.sepa.payment.service.commons.spring;

import eu.gentleart.sepa.payment.service.RestApiException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(RestApiException.class)
    public final ResponseEntity<ExceptionResponse> handleRestApiExceptions(RestApiException e) {
        return new ResponseEntity<>(new ExceptionResponse(e), e.getCode());
    }

}
