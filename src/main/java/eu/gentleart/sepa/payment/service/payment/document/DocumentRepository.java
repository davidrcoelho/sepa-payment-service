package eu.gentleart.sepa.payment.service.payment.document;

import eu.gentleart.sepa.payment.service.creditor.Creditor;
import eu.gentleart.sepa.payment.service.mandate.SequenceType;
import eu.gentleart.sepa.payment.service.tenant.Tenant;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface DocumentRepository extends CrudRepository<Document, Long> {

    Optional<Document> findByTenantAndCreditorAndSequenceTypeAndDate(Tenant tenant, Creditor creditor, SequenceType sequenceType, LocalDate today);

    List<Document> findByTenantAndStatus(Tenant tenant, DocumentStatus status);

}
