package eu.gentleart.sepa.payment.service.mandate;

import eu.gentleart.sepa.payment.service.commons.data.IntegrityViolationToDuplicatedAmendmentConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.transaction.Transactional;

@Controller
public class AmendmentController {

    private IntegrityViolationToDuplicatedAmendmentConverter exceptionConverter = new IntegrityViolationToDuplicatedAmendmentConverter();

    @Autowired
    private MandateRepository repository;

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public void createAmendment(Mandate existingMandate, Mandate newMandate) {

        newMandate.setPreviousMandate(existingMandate);
        newMandate.setAccount(existingMandate.getAccount());
        newMandate.setPreviousMandate(existingMandate);
        newMandate.setSequenceType(SequenceType.FIRST);
        newMandate.setStatus(MandateStatus.ACTIVE);

        exceptionConverter.execute(() -> repository.save(newMandate));

        existingMandate.setStatus(MandateStatus.DEACTIVE);
        exceptionConverter.execute(() -> repository.save(existingMandate));
    }

}
