package eu.gentleart.sepa.payment.service.payment;

import eu.gentleart.sepa.payment.service.api.exception.ResourceNotFoundException;
import eu.gentleart.sepa.payment.service.api.request.PaymentRequest;
import eu.gentleart.sepa.payment.service.creditor.Creditor;
import eu.gentleart.sepa.payment.service.creditor.CreditorFinder;
import eu.gentleart.sepa.payment.service.mandate.Mandate;
import eu.gentleart.sepa.payment.service.mandate.MandateController;
import eu.gentleart.sepa.payment.service.tenant.Tenant;

import java.util.Optional;

public class PaymentBuilder {

    private final MandateController mandateController;
    private final CreditorFinder creditorFinder;

    private PaymentRequest request;
    private String requestId;
    private Tenant tenant;

    public PaymentBuilder(MandateController mandateController, CreditorFinder creditorFinder) {
        this.mandateController = mandateController;
        this.creditorFinder = creditorFinder;
    }

    public PaymentBuilder withRequest(PaymentRequest request) {
        this.request = request;
        return this;
    }

    public PaymentBuilder withRequestId(String requestId) {
        this.requestId = requestId;
        return this;
    }

    public PaymentBuilder withTenant(Tenant tenant) {
        this.tenant = tenant;
        return this;
    }

    public Payment build() {

        Integer creditorId = request.getCreditorId();
        String accountClientId = request.getClientAccountId();

        Creditor creditor = creditorFinder.findCreditor(tenant, creditorId);
        Mandate mandate = findActiveMandateByAccountClientId(accountClientId);

        Payment payment = new Payment();
        payment.setTenant(tenant);
        payment.setMandate(mandate);
        payment.setRequestId(requestId);
        payment.setStatus(PaymentStatus.CREATED);
        payment.setCreditor(creditor);

        payment.setAmount(request.getAmount());
        payment.setBillingPurpose(request.getBillingPurpose());
        payment.setClientPaymentId(request.getClientPaymentId());
        payment.setRequestedCollectionDate(request.getRequestedCollectionDate());

        return payment;
    }

    private Mandate findActiveMandateByAccountClientId(String accountClientId) {
        Optional<Mandate> currentActiveMandateByAccount = mandateController.findActiveMandateByAccountClientId(accountClientId);

        if (!currentActiveMandateByAccount.isPresent()) {
            throw new ResourceNotFoundException("No mandate available for this account: " + accountClientId + ", tenant: " + tenant);
        }

        return currentActiveMandateByAccount.get();
    }

}
