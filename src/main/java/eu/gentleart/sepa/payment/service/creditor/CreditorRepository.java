package eu.gentleart.sepa.payment.service.creditor;

import eu.gentleart.sepa.payment.service.tenant.Tenant;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface CreditorRepository extends Repository<Creditor, Integer> {

    Optional<Creditor> findByTenantAndId(Tenant tenant, Integer creditorId);

    Creditor findByTenantAndCreditorKey(Tenant tenant, String creditorKey);

    Iterable<Creditor> findByTenant(Tenant tenant);

    @Modifying
    @Query("UPDATE Creditor c SET c.version = c.version + 1 where c.id = :#{#creditor.id} and c.version = :#{#creditor.version}")
    int lock(@Param("creditor") Creditor creditor);

}
