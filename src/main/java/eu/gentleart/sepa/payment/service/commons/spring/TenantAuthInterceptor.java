package eu.gentleart.sepa.payment.service.commons.spring;

import eu.gentleart.sepa.payment.service.tenant.Tenant;
import eu.gentleart.sepa.payment.service.tenant.TenantSessionStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class TenantAuthInterceptor implements HandlerInterceptor {

    private final TenantAuthValidator tenantAuthValidator;

    @Autowired
    public TenantAuthInterceptor(TenantAuthValidator tenantAuthValidator) {
        this.tenantAuthValidator = tenantAuthValidator;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        Tenant tenant = tenantAuthValidator.validate(request);
        TenantSessionStore.setCurrentTenant(tenant);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        TenantSessionStore.clear();
    }
}