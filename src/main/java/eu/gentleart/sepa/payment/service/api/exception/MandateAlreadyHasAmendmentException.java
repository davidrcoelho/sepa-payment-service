package eu.gentleart.sepa.payment.service.api.exception;

import eu.gentleart.sepa.payment.service.RestApiException;
import org.springframework.http.HttpStatus;

public class MandateAlreadyHasAmendmentException extends RestApiException {

    public MandateAlreadyHasAmendmentException() {
        super(HttpStatus.BAD_REQUEST, "Mandate already has amendment", "Request tried to add an amendment to a mandate that already has amendment");
    }

}
