package eu.gentleart.sepa.payment.service.payment;

public enum PaymentStatus {

    CREATED,
    SENT_TO_BANK

}
