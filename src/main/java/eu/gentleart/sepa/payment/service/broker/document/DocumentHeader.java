package eu.gentleart.sepa.payment.service.broker.document;

import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Getter
public class DocumentHeader {

    @NotBlank
    private String messageId;

    @NotNull
    private String creditorDateTime;

    public DocumentHeader(String messageId) {
        this.messageId = messageId;
        this.creditorDateTime = ZonedDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }
}
