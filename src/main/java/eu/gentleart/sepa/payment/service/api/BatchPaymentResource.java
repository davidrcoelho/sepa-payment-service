package eu.gentleart.sepa.payment.service.api;

import eu.gentleart.sepa.payment.service.api.request.PaymentRequest;
import eu.gentleart.sepa.payment.service.mandate.Mandate;
import eu.gentleart.sepa.payment.service.payment.PaymentController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static eu.gentleart.sepa.payment.service.PersistentController.HEADER_REQUEST_ID;

@RestController
public class BatchPaymentResource {

    private static final Logger logger = LoggerFactory.getLogger(BatchPaymentResource.class);

    @Autowired
    private PaymentController controller;

    @PostMapping("/v1/batch-payments")
    public ResponseEntity<Mandate> createPayments(
            @RequestHeader(HEADER_REQUEST_ID) String requestId,
            @RequestBody @Valid List<PaymentRequest> request) {

        logger.info("createBatchPayments. requestId: {}, request: {}", requestId, request);
        controller.createPayments(requestId, request);
        return new ResponseEntity(HttpStatus.OK);
    }

}
