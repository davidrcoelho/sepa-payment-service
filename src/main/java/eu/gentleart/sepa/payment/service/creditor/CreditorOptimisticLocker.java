package eu.gentleart.sepa.payment.service.creditor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CreditorOptimisticLocker {

    private static final Logger logger = LoggerFactory.getLogger(CreditorOptimisticLocker.class);

    @Autowired
    private CreditorRepository creditorRepository;

    public boolean quietlyLock(Creditor creditor) {
        int effectedRecords = creditorRepository.lock(creditor);

        if (effectedRecords == 0) {
            logger.info("It was not possible to lock the creditor: {}. No records was effect.", creditor);
        }

        return effectedRecords == 1;
    }

}
