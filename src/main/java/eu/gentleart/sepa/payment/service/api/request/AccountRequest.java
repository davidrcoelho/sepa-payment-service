package eu.gentleart.sepa.payment.service.api.request;

import eu.gentleart.sepa.payment.service.account.Account;
import lombok.Getter;
import lombok.ToString;

import java.util.Objects;

@Getter @ToString
public class AccountRequest {

    private String clientId;

    public Account toAccount(String requestId) {
        Objects.requireNonNull(requestId, "requestId cannot be null");

        Account account = new Account();
        account.setRequestId(requestId);
        account.setClientId(clientId);
        return account;
    }

    public Account toAccount(String requestId, Long accountId) {
        Objects.requireNonNull(accountId, "accountId cannot be null");

        Account account = toAccount(requestId);
        account.setId(accountId);
        return account;
    }


}
