package eu.gentleart.sepa.payment.service.payment;

import eu.gentleart.sepa.payment.service.creditor.Creditor;
import eu.gentleart.sepa.payment.service.mandate.SequenceType;
import eu.gentleart.sepa.payment.service.payment.document.Document;
import eu.gentleart.sepa.payment.service.tenant.Tenant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.time.ZonedDateTime;
import java.util.List;

public interface PaymentRepository extends CrudRepository<Payment, Long> {

    Payment save(Payment payment);

    @Query("select p from Payment p inner join p.mandate m where p.tenant = ?1 and p.creditor = ?2 and p.requestedCollectionDate <= ?3 and m.sequenceType = ?4 and p.document is null")
    List<Payment> findPaymentsWithoutDocument(Tenant tenant, Creditor creditor, ZonedDateTime requestedCollectionDate, SequenceType sequenceType);

    Page<Payment> findPaymentByDocument(Document document, Pageable pageable);
}
