package eu.gentleart.sepa.payment.service.payment.document;

import eu.gentleart.sepa.payment.service.creditor.Creditor;
import eu.gentleart.sepa.payment.service.creditor.CreditorOptimisticLocker;
import eu.gentleart.sepa.payment.service.mandate.SequenceType;
import eu.gentleart.sepa.payment.service.payment.Payment;
import eu.gentleart.sepa.payment.service.payment.PaymentRepository;
import eu.gentleart.sepa.payment.service.tenant.Tenant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

@Component
public class DocumentProcessor {

    private static final Logger logger = LoggerFactory.getLogger(DocumentProcessor.class);

    @Autowired
    private DocumentRepository documentRepository;

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private CreditorOptimisticLocker locker;

    @Value("${sepa.payments.jobs.pain08.first.scheduledDaysBeforeCollectionDate.default}")
    private long defaultScheduledDaysBeforeCollectionDate;

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public void processDocumentsBySequenceType(Tenant tenant, Creditor creditor, SequenceType sequenceType) {

        if (!locker.quietlyLock(creditor)) {
            logger.info("Documents for this Creditor are being processed by other process. Creditor: {}", creditor);
            return;
        }

        Document document = getOrCreateDocument(tenant, creditor, sequenceType);

        ZonedDateTime requestedCollectionDate = getRequestedCollectionDateForToday();
        List<Payment> payments = paymentRepository.findPaymentsWithoutDocument(tenant, creditor, requestedCollectionDate, sequenceType);

        logger.info("Payments to be added to the documents. payments.size: {}", payments.size());
        for (Payment payment : payments) {
            payment.setDocument(document);
            paymentRepository.save(payment);
        }

        if (!payments.isEmpty()) {
            document.setStatus(DocumentStatus.READY_TO_SEND);
            documentRepository.save(document);
        }

    }

    private Document getOrCreateDocument(Tenant tenant, Creditor creditor, SequenceType sequenceType) {
        Optional<Document> documentForToday = existsDocumentForToday(tenant, creditor, sequenceType);
        if (documentForToday.isPresent()) {
            logger.info("There is a document already created for today. sequenceType: {}, creditor: {}, tenant: {}", sequenceType, creditor, tenant);
            return documentForToday.get();
        } else {
            logger.info("Creating a new document . sequenceType: {}, creditor: {}, tenant: {}", sequenceType, creditor, tenant);
            Document document = new Document(tenant, creditor, sequenceType, DocumentStatus.NEW);
            return documentRepository.save(document);
        }
    }

    private Optional<Document> existsDocumentForToday(Tenant tenant, Creditor creditor, SequenceType sequenceType) {
        LocalDate today = LocalDate.now();
        return documentRepository.findByTenantAndCreditorAndSequenceTypeAndDate(tenant, creditor, sequenceType, today);
    }

    private ZonedDateTime getRequestedCollectionDateForToday() {
        ZonedDateTime now = ZonedDateTime.now();
        return now.plusDays(defaultScheduledDaysBeforeCollectionDate + 1);
    }


}
