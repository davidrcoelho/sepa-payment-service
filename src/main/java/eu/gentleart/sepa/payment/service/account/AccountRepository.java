package eu.gentleart.sepa.payment.service.account;

import eu.gentleart.sepa.payment.service.tenant.Tenant;
import org.springframework.data.repository.Repository;

import java.util.Optional;

public interface AccountRepository extends Repository<Account, Long> {

    Account save(Account account);

    Optional<Account> findByTenantAndId(Tenant tenant, long accountId);

    boolean existsByTenantAndId(Tenant tenant, long accountId);

    Account findByTenantAndRequestId(Tenant tenant, String requestId);

    Account findByTenantAndClientId(Tenant tenant, String clientId);
}
