package eu.gentleart.sepa.payment.service.commons.spring;

import eu.gentleart.sepa.payment.service.RestApiException;
import lombok.Getter;

import java.util.Date;

@Getter
public class ExceptionResponse {

    private Date date;
    private String message;
    private String description;

    public ExceptionResponse(RestApiException e) {
        this.date = new Date();
        this.message = e.getMessage();
        this.description = e.getDescription();
    }
}
