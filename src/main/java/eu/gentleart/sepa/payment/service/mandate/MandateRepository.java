package eu.gentleart.sepa.payment.service.mandate;

import eu.gentleart.sepa.payment.service.account.Account;
import eu.gentleart.sepa.payment.service.tenant.Tenant;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

public interface MandateRepository extends Repository<Mandate, Long> {

    Mandate save(Mandate mandate);

    List<Mandate> findAll();

    Mandate findByTenantAndRequestId(Tenant tenant, String requestId);

    Optional<Mandate> findByTenantAndAccountAndId(Tenant tenant, Account account, Long mandateId);

    Optional<Mandate> findByTenantAndAccountAndStatus(Tenant tenant, Account account, MandateStatus status);

    Optional<Mandate> findByTenantAndAccountAndRequestIdNot(Tenant tenant, Account account, String requestId);
}
