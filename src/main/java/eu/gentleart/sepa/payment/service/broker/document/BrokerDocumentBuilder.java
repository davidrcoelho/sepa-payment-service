package eu.gentleart.sepa.payment.service.broker.document;

import eu.gentleart.sepa.payment.service.broker.DirectDebitDocumentRequest;
import eu.gentleart.sepa.payment.service.creditor.Creditor;
import eu.gentleart.sepa.payment.service.payment.document.Document;

public class BrokerDocumentBuilder {

    public DirectDebitDocumentRequest build(Document document) {

        Creditor creditor = document.getCreditor();

        DocumentHeader documentHeader = new DocumentHeader(document.getBrokerMessageId());
        PaymentHeader paymentHeader = new PaymentHeader(creditor);

        return new DirectDebitDocumentRequest(documentHeader, paymentHeader);
    }

}
