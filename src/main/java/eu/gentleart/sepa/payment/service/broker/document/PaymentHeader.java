package eu.gentleart.sepa.payment.service.broker.document;

import eu.gentleart.sepa.payment.service.creditor.Creditor;
import lombok.Getter;

import javax.persistence.Embedded;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

@Getter
public class PaymentHeader {

    @NotBlank
    private String id;

    @NotBlank
    private String paymentMethod;

    @NotBlank
    private String requestedCollectionDate;

    @Valid
    @Embedded
    private BrokerCreditor creditor;

    public PaymentHeader(Creditor creditor) {
        this.id = UUID.randomUUID().toString();
        this.paymentMethod = "DD";
        this.requestedCollectionDate = ZonedDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);

        CreditorAccount creditorAccount = new CreditorAccount();
        creditorAccount.setIban(creditor.getIban());
        creditorAccount.setBic(creditor.getBic());

        BrokerCreditor brokerCreditor = new BrokerCreditor();
        brokerCreditor.setAccount(creditorAccount);
        brokerCreditor.setId(creditor.getId().toString());
        brokerCreditor.setSchemeId(creditor.getSchemeId());
        brokerCreditor.setName(creditor.getName());

        this.creditor = brokerCreditor;
    }

}
