package eu.gentleart.sepa.payment.service.account;

import eu.gentleart.sepa.payment.service.PersistentController;
import eu.gentleart.sepa.payment.service.api.exception.ResourceNotFoundException;
import eu.gentleart.sepa.payment.service.tenant.Tenant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.util.Optional;

@Controller
public class AccountController extends PersistentController<Account> {

    private AccountRepository repository;

    @Autowired
    public AccountController(AccountRepository repository) {
        this.repository = repository;
    }

    @Override
    public Account save(Account account) {
        return repository.save(account);
    }

    public boolean exists(Long accountId) {
        return repository.existsByTenantAndId(getCurrentTenant(), accountId);
    }

    @Override
    public Account findByRequestId(String requestId) {
        return repository.findByTenantAndRequestId(getCurrentTenant(), requestId);
    }

    public Account findById(long accountId) {
        Optional<Account> accountByTenantAndId = repository.findByTenantAndId(getCurrentTenant(), accountId);
        if (accountByTenantAndId.isPresent()) {
            return accountByTenantAndId.get();
        }
        throw new ResourceNotFoundException("Account.id not found: " + accountId);
    }

    public Account findByTenantAndClientId(Tenant tenant, String accountClientId) {
        return repository.findByTenantAndClientId(tenant, accountClientId);
    }

    public ResponseEntity<Account> updateOrGetEntityByRequestId(Account account, HttpStatus statusCode) {
        Long accountId = account.getId();
        if (!exists(accountId)) {
            throw new ResourceNotFoundException("Resource.id not found: " + accountId);
        }
        return persistOrGetEntityByRequestId(account, statusCode);
    }
}
