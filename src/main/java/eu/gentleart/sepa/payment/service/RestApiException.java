package eu.gentleart.sepa.payment.service;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter @Setter
public class RestApiException extends RuntimeException {

    private final HttpStatus code;
    private final String message;
    private final String description;

    public RestApiException(HttpStatus code, String message, String description) {
        super(message);
        this.code = code;
        this.message = message;
        this.description = description;
    }
}
