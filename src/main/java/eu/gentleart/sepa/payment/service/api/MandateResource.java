package eu.gentleart.sepa.payment.service.api;

import eu.gentleart.sepa.payment.service.account.AccountController;
import eu.gentleart.sepa.payment.service.api.request.MandateRequest;
import eu.gentleart.sepa.payment.service.mandate.Mandate;
import eu.gentleart.sepa.payment.service.mandate.MandateController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static eu.gentleart.sepa.payment.service.PersistentController.HEADER_REQUEST_ID;

@RestController
public class MandateResource {

    private static final Logger logger = LoggerFactory.getLogger(MandateResource.class);

    @Autowired
    private MandateController controller;

    @Autowired
    private AccountController accountController;

    @PostMapping("/v1/accounts/{id}/mandates")
    public ResponseEntity<Mandate> createMandate(
            @RequestHeader(HEADER_REQUEST_ID) String requestId,
            @PathVariable("id") long accountId,
            @RequestBody @Valid MandateRequest request) {

        logger.info("createMandate. requestId: {}, accountId: {}, request: {}", requestId, accountId, request);
        return controller.create(requestId, accountId, request);
    }

    @GetMapping("/v1/accounts/{id}/mandates/{mandateId}")
    public ResponseEntity<Mandate> getMandate(
            @PathVariable("id") long accountId,
            @PathVariable("mandateId") long mandateId) {

        logger.info("getMandate. accountId: {}, mandateId: {}", accountId, mandateId);
        Mandate mandate = controller.findByAccountAndMandateId(accountId, mandateId);
        return new ResponseEntity<>(mandate, HttpStatus.OK);
    }

}
