package eu.gentleart.sepa.payment.service.broker.payment;

import eu.gentleart.sepa.payment.service.mandate.Mandate;
import eu.gentleart.sepa.payment.service.payment.Payment;

public class BrokerPaymentBuilder {

    public BrokerPayment build(Payment payment) {

        Mandate mandate = payment.getMandate();

        BrokerPayment brokerPayment = new BrokerPayment();

        brokerPayment.setSequenceType(mandate.getSequenceType().getValue());
        brokerPayment.setIdentification(Identification.of(payment.getId()));
        brokerPayment.setInstructedAmount(InstructedAmount.of(payment.getAmount()));
        brokerPayment.setInstitutionIdentification(InstitutionIdentification.of(mandate.getBic()));
        brokerPayment.setDebitorAccount(DebitorAccount.of(mandate));
        brokerPayment.setRemittance(Remittance.of(payment.getBillingPurpose()));
        brokerPayment.setMandate(BrokerMandate.of(payment, mandate));

        return brokerPayment;

    }

}
