package eu.gentleart.sepa.payment.service;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

@MappedSuperclass
@Getter @Setter
public class ResourceEntity extends MultiTenantEntity<Long> {

    @NotNull
    @Setter
    @Column(unique = true)
    @JsonIgnore
    private String requestId;

}
