package eu.gentleart.sepa.payment.service.payment;

import eu.gentleart.sepa.payment.service.api.request.PaymentRequest;
import eu.gentleart.sepa.payment.service.creditor.CreditorFinder;
import eu.gentleart.sepa.payment.service.mandate.MandateController;
import eu.gentleart.sepa.payment.service.tenant.TenantSessionStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class PaymentController {

    private static final Logger logger = LoggerFactory.getLogger(PaymentController.class);

    @Autowired
    private PaymentRepository repository;

    @Autowired
    private MandateController mandateController;

    @Autowired
    private CreditorFinder creditorFinder;

    public void createPayments(String requestId, List<PaymentRequest> requestList) {

        logger.info("Processing list of payment request. size: {}", requestList.size());

        for (PaymentRequest request : requestList) {

            Payment payment = new PaymentBuilder(mandateController, creditorFinder)
                    .withTenant(TenantSessionStore.getCurrentTenant())
                    .withRequest(request)
                    .withRequestId(requestId)
                    .build();

            repository.save(payment);
        }
    }

}
