package eu.gentleart.sepa.payment.service.tenant;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TenantRepository extends CrudRepository<Tenant, Integer> {

    Optional<Tenant> findByUsername(String name);

    Iterable<Tenant> findAll();

}
