package eu.gentleart.sepa.payment.service.mandate;

public enum SequenceType {

    FIRST("FRST"),
    RECURRING("RCURR");

    private String value;

    SequenceType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
