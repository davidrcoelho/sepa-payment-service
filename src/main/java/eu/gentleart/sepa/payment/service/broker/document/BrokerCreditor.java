package eu.gentleart.sepa.payment.service.broker.document;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Embedded;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Getter @Setter @ToString
public class BrokerCreditor {

    private String id;

    private String schemeId;

    @NotBlank
    private String name;

    @Valid
    @Embedded
    private CreditorAccount account;

}
