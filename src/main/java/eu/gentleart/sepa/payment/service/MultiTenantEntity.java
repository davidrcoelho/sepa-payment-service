package eu.gentleart.sepa.payment.service;

import com.fasterxml.jackson.annotation.JsonIgnore;
import eu.gentleart.sepa.payment.service.tenant.Tenant;
import eu.gentleart.sepa.payment.service.tenant.TenantSessionStore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@MappedSuperclass
@FilterDef(name = "tenantFilter", parameters = {@ParamDef(name = "tenantId", type = "string")})
@Filter(name = "tenantFilter", condition = "tenant_id2 = :tenantId2")
public abstract class MultiTenantEntity<T> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter
    private T id;

    @Version
    @Getter
    @JsonIgnore
    private long version;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creation_date", updatable = false)
    @JsonIgnore
    private Date creationDate;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_modified_date")
    @JsonIgnore
    private Date lastModifiedDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @NotNull
    @JsonIgnore
    private Tenant tenant;

    protected void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    @PrePersist
    void prePersist() {
        setTenant();
    }

    @PreUpdate
    void preUpdate() {
        setTenant();
    }

    private void setTenant() {
        if (tenant == null) {
            tenant = TenantSessionStore.getCurrentTenant();
        }
    }

}
