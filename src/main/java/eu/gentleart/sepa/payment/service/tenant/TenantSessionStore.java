package eu.gentleart.sepa.payment.service.tenant;

public class TenantSessionStore {

    private static ThreadLocal<Tenant> currentTenant = new ThreadLocal<>();

    private TenantSessionStore() {}

    public static Tenant getCurrentTenant() {
        return currentTenant.get();
    }

    public static void setCurrentTenant(Tenant tenant) {
        currentTenant.set(tenant);
    }

    public static void clear() {
        currentTenant.remove();
    }

}
