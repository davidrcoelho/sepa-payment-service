package eu.gentleart.sepa.payment.service.payment;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import eu.gentleart.sepa.payment.service.ResourceEntity;
import eu.gentleart.sepa.payment.service.creditor.Creditor;
import eu.gentleart.sepa.payment.service.mandate.Mandate;
import eu.gentleart.sepa.payment.service.payment.document.Document;
import eu.gentleart.sepa.payment.service.tenant.Tenant;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Entity
@Getter @Setter @ToString
public class Payment extends ResourceEntity {

    @ManyToOne
    @JoinColumn(name = "mandate_id")
    @NotNull
    @JsonBackReference
    private Mandate mandate;

    @OneToOne
    @JoinColumn(name = "creditor_id")
    private Creditor creditor;

    @NotNull
    private String clientPaymentId;

    private ZonedDateTime requestedCollectionDate;

    @Embedded
    private Amount amount;

    private String billingPurpose;

    @Enumerated(EnumType.STRING)
    private PaymentStatus status;

    @ManyToOne
    @JoinColumn(name = "document_id")
    private Document document;

    @Override
    public void setTenant(Tenant tenant) {
        super.setTenant(tenant);
    }

    public boolean equalsStatus(PaymentStatus status) {
        return status.equals(this.status);
    }

    @JsonIgnore
    public String getRequestedCollectionDateAsString() {
        return requestedCollectionDate.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }
}
