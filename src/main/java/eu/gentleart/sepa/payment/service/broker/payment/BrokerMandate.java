package eu.gentleart.sepa.payment.service.broker.payment;

import eu.gentleart.sepa.payment.service.mandate.Mandate;
import eu.gentleart.sepa.payment.service.payment.Payment;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class BrokerMandate {
    private String umr;
    private String dateOfSignature;
    private String collectionDate;

    public static BrokerMandate of(Payment payment, Mandate mandate) {
        BrokerMandate brokerMandate = new BrokerMandate();
        brokerMandate.setUmr(mandate.getUmr());
        brokerMandate.setDateOfSignature(mandate.getDateOfSignatureAsString());
        brokerMandate.setCollectionDate(payment.getRequestedCollectionDateAsString());
        return brokerMandate;
    }

}
