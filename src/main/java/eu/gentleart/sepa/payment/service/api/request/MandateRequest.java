package eu.gentleart.sepa.payment.service.api.request;

import eu.gentleart.sepa.payment.service.account.Account;
import eu.gentleart.sepa.payment.service.mandate.Mandate;
import lombok.Getter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.util.Objects;

@Getter @ToString
public class MandateRequest {

    @NotNull
    private String name;

    @NotNull
    private String iban;

    @NotNull
    private String bic;

    @NotNull
    private ZonedDateTime dateOfSignature;

    public Mandate getData(String requestId, Account account) {

        Objects.requireNonNull(requestId, "requestId cannot be null");
        Objects.requireNonNull(account, "account cannot be null");

        Mandate mandate = new Mandate();
        mandate.setRequestId(requestId);
        mandate.setAccount(account);

        mandate.setDateOfSignature(dateOfSignature);
        mandate.setName(name);
        mandate.setIban(iban);
        mandate.setBic(bic);

        return mandate;
    }


}
