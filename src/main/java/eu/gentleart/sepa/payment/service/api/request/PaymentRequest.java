package eu.gentleart.sepa.payment.service.api.request;

import eu.gentleart.sepa.payment.service.payment.Amount;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@Getter
public class PaymentRequest {

    @NotNull
    private String clientPaymentId;

    @NotNull
    private String clientAccountId;

    private Integer creditorId;

    @NotNull
    private ZonedDateTime requestedCollectionDate;

    @NotNull
    private String billingPurpose;

    private Amount amount;

}
