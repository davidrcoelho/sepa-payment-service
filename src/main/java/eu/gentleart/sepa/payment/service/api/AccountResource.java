package eu.gentleart.sepa.payment.service.api;

import eu.gentleart.sepa.payment.service.account.Account;
import eu.gentleart.sepa.payment.service.account.AccountController;
import eu.gentleart.sepa.payment.service.api.request.AccountRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static eu.gentleart.sepa.payment.service.PersistentController.HEADER_REQUEST_ID;

@RestController
public class AccountResource {

    private static final Logger logger = LoggerFactory.getLogger(AccountResource.class);

    @Autowired
    private AccountController controller;

    @PostMapping("/v1/accounts")
    public ResponseEntity<Account> createAccount(
            @RequestHeader(HEADER_REQUEST_ID) String requestId,
            @RequestBody AccountRequest request) {

        logger.info("createAccount. requestId: {}, request: {}", requestId, request);

        Account account = request.toAccount(requestId);
        return controller.persistOrGetEntityByRequestId(account, HttpStatus.CREATED);
    }

    @GetMapping("/v1/accounts/{id}")
    public ResponseEntity<Account> getAccount(
            @PathVariable("id") long accountId) {

        logger.info("getAccount. accountId: {}", accountId);
        Account account = controller.findById(accountId);
        return new ResponseEntity(account, HttpStatus.OK);
    }

    @PutMapping("/v1/accounts/{id}")
    public ResponseEntity<Account> updateAccount(
            @RequestHeader(HEADER_REQUEST_ID) String requestId,
            @PathVariable("id") long accountId,
            @RequestBody AccountRequest request) {

        logger.info("updateAccount. requestId: {}, request: {}, accountId: {}", requestId, request, accountId);

        Account account = request.toAccount(requestId, accountId);
        return controller.updateOrGetEntityByRequestId(account, HttpStatus.OK);
    }

}
