package eu.gentleart.sepa.payment.service.broker;

import eu.gentleart.sepa.payment.service.broker.document.DocumentHeader;
import eu.gentleart.sepa.payment.service.broker.document.PaymentHeader;
import lombok.Getter;

import javax.validation.Valid;

@Getter
public class DirectDebitDocumentRequest {

    @Valid
    private DocumentHeader documentHeader;

    @Valid
    private PaymentHeader paymentHeader;

    public DirectDebitDocumentRequest(@Valid DocumentHeader documentHeader, @Valid PaymentHeader paymentHeader) {
        this.documentHeader = documentHeader;
        this.paymentHeader = paymentHeader;
    }
}
