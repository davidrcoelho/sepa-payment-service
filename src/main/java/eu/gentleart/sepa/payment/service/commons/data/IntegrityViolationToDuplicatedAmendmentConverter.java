package eu.gentleart.sepa.payment.service.commons.data;

import eu.gentleart.sepa.payment.service.api.exception.MandateAlreadyHasAmendmentException;
import org.springframework.dao.DataIntegrityViolationException;

public class IntegrityViolationToDuplicatedAmendmentConverter {

    public interface CustomCallable {
        Object call();
    }

    private static final String UNIQUE_MANDATE_ID_CONSTRAINT = "unique_previous_mandate_id";

    public void execute(CustomCallable callable) {
        try {
            callable.call();
        } catch (DataIntegrityViolationException e) {
            if (containsMessageFragment(e)) {
                throw new MandateAlreadyHasAmendmentException();
            }
            throw e;
        }

    }

    private boolean containsMessageFragment(DataIntegrityViolationException e) {
        String errorMessage = getErrorMessage(e);
        return errorMessage.contains(UNIQUE_MANDATE_ID_CONSTRAINT);
    }

    private String getErrorMessage(DataIntegrityViolationException e) {
        StringBuilder message = new StringBuilder();

        Throwable rootCause = e.getRootCause();
        if (rootCause != null) {
            message.append(rootCause.getMessage());
            message.append(" - ");
        }

        message.append(e.getMessage());
        return message.toString();
    }

}
