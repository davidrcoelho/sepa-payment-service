package eu.gentleart.sepa.payment.service.commons.spring;

import eu.gentleart.sepa.payment.service.tenant.TenantRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TenantAuthValidatorConfig {

    @Bean
    public TenantAuthValidator tenantAuthValidator(TenantRepository repository) {
        return new TenantAuthValidator(repository);
    }

}
