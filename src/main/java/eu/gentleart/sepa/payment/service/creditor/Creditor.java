package eu.gentleart.sepa.payment.service.creditor;

import eu.gentleart.sepa.payment.service.MultiTenantEntity;
import lombok.Getter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Getter @ToString
public class Creditor extends MultiTenantEntity<Integer> {

    @Column(name = "account_iban", updatable = false)
    private String iban;

    @Column(name = "account_bic", updatable = false)
    private String bic;

    @Column(name = "account_name", updatable = false)
    private String name;

    @Column(name = "scheme_id", updatable = false)
    private String schemeId;

    @Column(name = "creditor_key", updatable = false)
    private String creditorKey;

}
