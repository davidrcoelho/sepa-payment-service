package eu.gentleart.sepa.payment.service.api.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import eu.gentleart.sepa.payment.service.mandate.Mandate;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Getter
public class AmendmentResponse {

    private Mandate root;
    private final List<Mandate> previousMandates = new ArrayList<>();

    public AmendmentResponse(Mandate mandateRoot) {
        this.root = mandateRoot;

        Mandate current = mandateRoot;
        while (current.hasPreviousMandate()) {
            current = current.getPreviousMandate();
            previousMandates.add(current);
        }

    }

}
