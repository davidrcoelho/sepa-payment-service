package eu.gentleart.sepa.payment.service.broker;

import com.jayway.jsonpath.JsonPath;
import eu.gentleart.sepa.payment.service.broker.document.BrokerDocumentBuilder;
import eu.gentleart.sepa.payment.service.broker.payment.BrokerPayment;
import eu.gentleart.sepa.payment.service.broker.payment.BrokerPaymentBuilder;
import eu.gentleart.sepa.payment.service.payment.Payment;
import eu.gentleart.sepa.payment.service.payment.PaymentRepository;
import eu.gentleart.sepa.payment.service.payment.document.Document;
import eu.gentleart.sepa.payment.service.payment.document.DocumentRepository;
import eu.gentleart.sepa.payment.service.payment.document.DocumentStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class BrokerFacade {

    private static final Logger logger = LoggerFactory.getLogger(BrokerFacade.class);

    @Value("${sepa.broker.documents.url}")
    private String documentsUrl;

    @Autowired
    private HttpBrokerClient brokerClient;

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private DocumentRepository documentRepository;

    public void send(Document document) throws IOException {
        DirectDebitDocumentRequest request = new BrokerDocumentBuilder().build(document);

        String requestId = document.getBrokerRequestId();

        String response = brokerClient.post(documentsUrl, requestId, request);
        logger.info("Response: {}", response);

        Object brokerDocumentId = (JsonPath.read(response, "id"));
        String paymentsUrl = documentsUrl + "/" + brokerDocumentId + "/payments";

        Page<Payment> paymentByDocument = paymentRepository.findPaymentByDocument(document, PageRequest.of(0, 10000));
        while (!paymentByDocument.isEmpty()) {

            logger.info("Sending payments to Broker. payments: {}", paymentByDocument);
            for (Payment payment : paymentByDocument.getContent()) {
                BrokerPayment paymentRequest = new BrokerPaymentBuilder().build(payment);
                brokerClient.post(paymentsUrl, payment.getRequestId(), paymentRequest);
            }

            if (paymentByDocument.isLast()) {
                break;
            }

            Pageable pageable = paymentByDocument.nextPageable();
            paymentByDocument = paymentRepository.findPaymentByDocument(document, pageable);
        }

        document.setStatus(DocumentStatus.SENT_TO_BROKER);
        document.setBrokerDocumentId(String.valueOf(brokerDocumentId));
        documentRepository.save(document);
    }

}
