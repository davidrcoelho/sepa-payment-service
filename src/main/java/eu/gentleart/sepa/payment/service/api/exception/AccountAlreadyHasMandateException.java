package eu.gentleart.sepa.payment.service.api.exception;

import eu.gentleart.sepa.payment.service.RestApiException;
import org.springframework.http.HttpStatus;

public class AccountAlreadyHasMandateException extends RestApiException {

    public AccountAlreadyHasMandateException() {
        super(HttpStatus.BAD_REQUEST, "Account already has a mandate", "It is not possible to include multiple mandates via POST /mandates. Use /amendments resource instead.");
    }

}
