package eu.gentleart.sepa.payment.service.tenant;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "tenant")
@Getter @Setter @ToString
public class Tenant {

    @Id
    private int id;

    @Column(name = "name", unique = true, length = 100)
    private String name;

    @Column(name = "username", unique = true, length = 100)
    private String username;

    @Column(name = "tenant_key", unique = true, length = 100)
    private String tenantKey;

    @Version
    private long version;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creation_date")
    private Date creationDate;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_modified_date")
    private Date lastModifiedDate;

}
