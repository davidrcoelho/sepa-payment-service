package eu.gentleart.sepa.payment.service.mandate;

import eu.gentleart.sepa.payment.service.PersistentController;
import eu.gentleart.sepa.payment.service.account.Account;
import eu.gentleart.sepa.payment.service.account.AccountController;
import eu.gentleart.sepa.payment.service.api.exception.AccountAlreadyHasMandateException;
import eu.gentleart.sepa.payment.service.api.exception.ResourceNotFoundException;
import eu.gentleart.sepa.payment.service.api.request.MandateRequest;
import eu.gentleart.sepa.payment.service.api.response.AmendmentResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.util.Optional;

@Controller
public class MandateController extends PersistentController<Mandate> {

    private final MandateRepository repository;
    private final AccountController accountController;

    @Autowired
    public MandateController(MandateRepository repository, AccountController accountController) {
        this.repository = repository;
        this.accountController = accountController;
    }

    @Override
    public Mandate save(Mandate mandate) {
        return repository.save(mandate);
    }

    @Override
    public Mandate findByRequestId(String requestId) {
        return repository.findByTenantAndRequestId(getCurrentTenant(), requestId);
    }

    public Mandate findByAccountAndMandateId(long accountId, long mandateId) {
        Account account = accountController.findById(accountId);

        Optional<Mandate> mandateByAccountAndId = repository.findByTenantAndAccountAndId(getCurrentTenant(), account, mandateId);

        if (mandateByAccountAndId.isPresent()) {
            return mandateByAccountAndId.get();
        }

        throw new ResourceNotFoundException("Mandate not found. account: " + account + ", mandateId: " + mandateId);
    }

    public AmendmentResponse listAmendmentsByAccountAndMandateId(long accountId, long mandateId) {
        Mandate mandate = findByAccountAndMandateId(accountId, mandateId);
        return new AmendmentResponse(mandate);
    }

    public Optional<Mandate> findByAccountAndRequestIdNot(Account account, String requestId) {
        return repository.findByTenantAndAccountAndRequestIdNot(getCurrentTenant(), account, requestId);
    }

    public Optional<Mandate> findActiveMandateByAccountClientId(String accountClientId) {
        Account account = accountController.findByTenantAndClientId(getCurrentTenant(), accountClientId);
        return repository.findByTenantAndAccountAndStatus(getCurrentTenant(), account, MandateStatus.ACTIVE);
    }

    public ResponseEntity<Mandate> create(String requestId, long accountId, MandateRequest request) {

        Account account = accountController.findById(accountId);
        Mandate mandate = request.getData(requestId, account);

        mandate.setSequenceType(SequenceType.FIRST);
        mandate.setStatus(MandateStatus.ACTIVE);

        Optional<Mandate> mandateByAccount = findByAccountAndRequestIdNot(account, requestId);
        if (mandateByAccount.isPresent()) {
            throw new AccountAlreadyHasMandateException();
        }

        return persistOrGetEntityByRequestId(mandate, HttpStatus.CREATED);
    }

}
