package eu.gentleart.sepa.payment.service.broker.payment;

import eu.gentleart.sepa.payment.service.payment.Amount;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class InstructedAmount {
    private String currency;
    private BigDecimal value;

    public static InstructedAmount of(Amount amount) {
        InstructedAmount instructedAmount = new InstructedAmount();
        instructedAmount.setCurrency(amount.getCurrency());
        instructedAmount.setValue(amount.getValue());
        return instructedAmount;
    }

}
