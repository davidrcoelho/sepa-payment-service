package eu.gentleart.sepa.payment.service.mandate;

public class IllegalMandateStatusException extends RuntimeException {

    public IllegalMandateStatusException() {
        super("Cannot change the mandate status");
    }

}
