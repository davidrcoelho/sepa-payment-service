package eu.gentleart.sepa.payment.service.broker.payment;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Remittance {
    private String unstructured;

    public static Remittance of(String unstructured) {
        Remittance remittance = new Remittance();
        remittance.setUnstructured(unstructured);
        return remittance;
    }
}
