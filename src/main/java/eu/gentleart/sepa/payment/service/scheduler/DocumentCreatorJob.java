package eu.gentleart.sepa.payment.service.scheduler;

import eu.gentleart.sepa.payment.service.payment.document.DocumentCreator;
import eu.gentleart.sepa.payment.service.payment.document.DocumentSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class DocumentCreatorJob {

    private static final Logger logger = LoggerFactory.getLogger(DocumentCreatorJob.class);

    @Autowired
    private DocumentCreator documentCreator;

    @Autowired
    private DocumentSender documentSender;

    @Scheduled(fixedDelayString = "${sepa.payments.jobs.documents.fixedDelayInMillis}")
    public void scheduled() throws IOException {
        logger.info("Processing document creation job");
        documentCreator.createDocuments();
        documentSender.sendReadyDocumentsToBroker();
    }

}

