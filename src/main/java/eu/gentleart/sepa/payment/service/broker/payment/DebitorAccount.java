package eu.gentleart.sepa.payment.service.broker.payment;

import eu.gentleart.sepa.payment.service.mandate.Mandate;
import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class DebitorAccount {

    private String name;
    private String iban;
    private String bic;

    public static DebitorAccount of(Mandate mandate) {
        DebitorAccount debitorAccount = new DebitorAccount();
        debitorAccount.setName(mandate.getName());
        debitorAccount.setIban(mandate.getIban());
        debitorAccount.setBic(mandate.getBic());
        return debitorAccount;
    }

}
