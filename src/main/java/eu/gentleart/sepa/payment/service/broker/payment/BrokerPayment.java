package eu.gentleart.sepa.payment.service.broker.payment;

import lombok.Getter;
import lombok.Setter;


@Getter @Setter
public class BrokerPayment {
    private String sequenceType;
    private Identification identification;
    private InstructedAmount instructedAmount;
    private InstitutionIdentification institutionIdentification;
    private DebitorAccount debitorAccount;
    private Remittance remittance;
    private BrokerMandate mandate;
}


