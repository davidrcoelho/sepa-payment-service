package eu.gentleart.sepa.payment.service.api;

import eu.gentleart.sepa.payment.service.account.Account;
import eu.gentleart.sepa.payment.service.account.AccountController;
import eu.gentleart.sepa.payment.service.api.request.MandateRequest;
import eu.gentleart.sepa.payment.service.api.response.AmendmentResponse;
import eu.gentleart.sepa.payment.service.mandate.AmendmentController;
import eu.gentleart.sepa.payment.service.mandate.Mandate;
import eu.gentleart.sepa.payment.service.mandate.MandateController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static eu.gentleart.sepa.payment.service.PersistentController.HEADER_REQUEST_ID;

@RestController
public class AmendmentResource {

    private static final Logger logger = LoggerFactory.getLogger(AmendmentResource.class);

    @Autowired
    private AmendmentController controller;

    @Autowired
    private MandateController mandateController;

    @Autowired
    private AccountController accountController;

    @PostMapping("/v1/accounts/{id}/mandates/{mandateId}/amendments")
    public ResponseEntity<Mandate> createAmendment(
            @RequestHeader(HEADER_REQUEST_ID) String requestId,
            @PathVariable("id") long accountId,
            @PathVariable("mandateId") long mandateId,
            @RequestBody @Valid MandateRequest request) {

        logger.info("createAmendment. requestId: {}, accountId: {}, request: {}", requestId, accountId, request);

        Mandate existingMandate = mandateController.findByAccountAndMandateId(accountId, mandateId);
        Account account = existingMandate.getAccount();
        Mandate newMandate = request.getData(requestId, account);

        controller.createAmendment(existingMandate, newMandate);

        return new ResponseEntity(newMandate, HttpStatus.CREATED);
    }

    @GetMapping("/v1/accounts/{id}/mandates/{mandateId}/amendments")
    public ResponseEntity<AmendmentResponse> getAmendments(
            @PathVariable("id") long accountId,
            @PathVariable("mandateId") long mandateId) {

        logger.info("getAmendments. accountId: {}", accountId);

        AmendmentResponse amendmentResponse = mandateController.listAmendmentsByAccountAndMandateId(accountId, mandateId);
        return new ResponseEntity(amendmentResponse, HttpStatus.OK);
    }

}
