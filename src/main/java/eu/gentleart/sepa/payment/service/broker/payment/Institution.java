package eu.gentleart.sepa.payment.service.broker.payment;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Institution {
    private String bic;

}
