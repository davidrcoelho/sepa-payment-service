package eu.gentleart.sepa.payment.service.tenant;

import eu.gentleart.sepa.payment.service.RestApiException;
import org.springframework.http.HttpStatus;

public class TenantNotFoundException extends RestApiException {

    public TenantNotFoundException() {
        super(HttpStatus.UNAUTHORIZED, "Unauthorized", "Not possible to identify the tenant based on the logged user");
    }
}
