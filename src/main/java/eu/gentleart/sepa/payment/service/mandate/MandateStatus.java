package eu.gentleart.sepa.payment.service.mandate;

public enum MandateStatus {

    ACTIVE,
    DEACTIVE

}
