package eu.gentleart.sepa.payment.service.api.exception;

import eu.gentleart.sepa.payment.service.RestApiException;
import org.springframework.http.HttpStatus;

public class ResourceNotFoundException extends RestApiException {

    public ResourceNotFoundException(String description) {
        super(HttpStatus.NOT_FOUND, "Resource not found", description);
    }
}
