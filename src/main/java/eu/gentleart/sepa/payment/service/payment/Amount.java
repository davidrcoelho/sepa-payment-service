package eu.gentleart.sepa.payment.service.payment;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.math.BigDecimal;

@Embeddable
@Getter @NoArgsConstructor
public class Amount {

    @Column(name = "amount_currency", length = 5)
    private String currency;

    @Column(name = "amount_value")
    private BigDecimal value;

}
