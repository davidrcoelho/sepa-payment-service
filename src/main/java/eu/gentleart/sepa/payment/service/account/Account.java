package eu.gentleart.sepa.payment.service.account;

import eu.gentleart.sepa.payment.service.ResourceEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity(name = "account")
@Getter @ToString
public class Account extends ResourceEntity {

    @NotNull
    @Setter
    private String clientId;

}
