package eu.gentleart.sepa.payment.service.payment.document;

import eu.gentleart.sepa.payment.service.MultiTenantEntity;
import eu.gentleart.sepa.payment.service.creditor.Creditor;
import eu.gentleart.sepa.payment.service.mandate.SequenceType;
import eu.gentleart.sepa.payment.service.tenant.Tenant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import java.time.LocalDate;
import java.util.UUID;

@Entity
@Getter @NoArgsConstructor
public class Document extends MultiTenantEntity<Long> {

    @ManyToOne
    private Creditor creditor;

    private LocalDate date;

    @Enumerated(EnumType.STRING)
    private SequenceType sequenceType;

    @Enumerated(EnumType.STRING)
    @Setter
    private DocumentStatus status;

    private String brokerMessageId;

    private String brokerRequestId;

    @Setter
    private String brokerDocumentId;

    public Document(Tenant tenant, Creditor creditor, SequenceType sequenceType, DocumentStatus status) {
        setTenant(tenant);
        this.creditor = creditor;
        this.sequenceType = sequenceType;
        this.status = status;
        this.brokerMessageId = UUID.randomUUID().toString();
        this.brokerRequestId = UUID.randomUUID().toString();
        this.date = LocalDate.now();
    }

}