package eu.gentleart.sepa.payment.service.creditor;

import eu.gentleart.sepa.payment.service.api.exception.ResourceNotFoundException;
import eu.gentleart.sepa.payment.service.tenant.Tenant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;

import java.util.Optional;

@Controller
public class CreditorFinder {

    public static final String SEPA_PAYMENTS_DEFAULT_CREDITOR = "sepa.payments.defaultCreditor.";

    @Autowired
    private CreditorRepository creditorRepository;

    @Autowired
    private Environment environment;

    public Creditor findCreditor(Tenant tenant, Integer creditorId) {

        if (creditorId == null) {
            String creditorKey = getDefaultCreditorKey(tenant);
            return creditorRepository.findByTenantAndCreditorKey(tenant, creditorKey);
        }

        Optional<Creditor> creditorById = creditorRepository.findByTenantAndId(tenant, creditorId);
        if (creditorById.isPresent()) {
            return creditorById.get();
        }

        throw new ResourceNotFoundException("Creditor not found with id: " + creditorId);

    }

    private String getDefaultCreditorKey(Tenant tenant) {
        return environment.getProperty(SEPA_PAYMENTS_DEFAULT_CREDITOR + tenant.getTenantKey());
    }

}
