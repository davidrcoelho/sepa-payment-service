package eu.gentleart.sepa.payment.service.payment.document;

import eu.gentleart.sepa.payment.service.creditor.Creditor;
import eu.gentleart.sepa.payment.service.creditor.CreditorRepository;
import eu.gentleart.sepa.payment.service.mandate.SequenceType;
import eu.gentleart.sepa.payment.service.tenant.Tenant;
import eu.gentleart.sepa.payment.service.tenant.TenantRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DocumentCreator {

    private static final Logger logger = LoggerFactory.getLogger(DocumentCreator.class);

    @Autowired
    private TenantRepository tenantRepository;

    @Autowired
    private CreditorRepository creditorRepository;

    @Autowired
    private DocumentProcessor documentProcessor;

    public void createDocuments() {

        logger.info("Creating documents");

        Iterable<Tenant> allTenants = tenantRepository.findAll();
        for (Tenant tenant : allTenants) {

            Iterable<Creditor> creditors = creditorRepository.findByTenant(tenant);

            for (Creditor creditor : creditors) {

                for (SequenceType sequenceType : SequenceType.values()) {
                    logger.info("Processing document for sequenceType: {}", sequenceType);
                    documentProcessor.processDocumentsBySequenceType(tenant, creditor, sequenceType);
                }

            }
        }

    }

}
