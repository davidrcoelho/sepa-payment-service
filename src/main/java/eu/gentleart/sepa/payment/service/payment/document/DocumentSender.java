package eu.gentleart.sepa.payment.service.payment.document;

import eu.gentleart.sepa.payment.service.broker.BrokerFacade;
import eu.gentleart.sepa.payment.service.tenant.Tenant;
import eu.gentleart.sepa.payment.service.tenant.TenantRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class DocumentSender {

    private static final Logger logger = LoggerFactory.getLogger(DocumentSender.class);

    @Autowired
    private TenantRepository tenantRepository;

    @Autowired
    private DocumentRepository documentRepository;

    @Autowired
    private BrokerFacade brokerFacade;

    public void sendReadyDocumentsToBroker() throws IOException {

        Iterable<Tenant> allTenants = tenantRepository.findAll();
        for (Tenant tenant : allTenants) {
            List<Document> documents = documentRepository.findByTenantAndStatus(tenant, DocumentStatus.READY_TO_SEND);

            logger.info("Sending documents to Broker. documents.size: {}", documents.size());
            for (Document document : documents) {
                brokerFacade.send(document);
            }

        }
    }

}
