package eu.gentleart.sepa.payment.service.commons.spring;

import eu.gentleart.sepa.payment.service.tenant.Tenant;
import eu.gentleart.sepa.payment.service.tenant.TenantNotFoundException;
import eu.gentleart.sepa.payment.service.tenant.TenantRepository;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Optional;

public class TenantAuthValidator {

    private static final Logger logger = LoggerFactory.getLogger(TenantAuthValidator.class);

    protected TenantRepository repository;

    public TenantAuthValidator(TenantRepository repository) {
        this.repository = repository;
    }

    public Tenant validate(HttpServletRequest request) {

        Principal principal = request.getUserPrincipal();

        String username = getUsername(principal);

        logger.info("Finding the username based on login username: {}", username);

        Optional<Tenant> tenantOptional = repository.findByUsername(username);

        if (tenantOptional.isPresent()) {
            return tenantOptional.get();
        }

        logger.warn("Tenant not found at the database with username: {}", username);
        throw new TenantNotFoundException();

    }

    private String getUsername(Principal principal) {

        if (principal instanceof KeycloakPrincipal) {
            KeycloakPrincipal<KeycloakSecurityContext> kp = (KeycloakPrincipal<KeycloakSecurityContext>) principal;
            AccessToken token = kp.getKeycloakSecurityContext().getToken();
            return token.getPreferredUsername();
        }

        logger.warn("Error on getting username from http request.");
        throw new TenantNotFoundException();
    }


}
