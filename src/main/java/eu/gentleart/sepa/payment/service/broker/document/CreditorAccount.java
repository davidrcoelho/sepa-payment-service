package eu.gentleart.sepa.payment.service.broker.document;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;

@Embeddable
@Getter @Setter @ToString
public class CreditorAccount {

    @NotBlank
    private String iban;

    @NotBlank
    private String bic;
}
