package eu.gentleart.sepa.payment.service.mandate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import eu.gentleart.sepa.payment.service.ResourceEntity;
import eu.gentleart.sepa.payment.service.account.Account;
import eu.gentleart.sepa.payment.service.payment.Payment;
import eu.gentleart.sepa.payment.service.payment.PaymentStatus;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static eu.gentleart.sepa.payment.service.mandate.MandateStatus.DEACTIVE;
import static org.hibernate.type.IntegerType.ZERO;

@Entity(name = "mandate")
@Getter @Setter @ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Mandate extends ResourceEntity {

    @Column(name = "date_of_signature")
    private ZonedDateTime dateOfSignature;

    @NotBlank
    private String name;

    @NotBlank
    private String iban;

    @NotBlank
    private String bic;

    @ManyToOne
    @JoinColumn(name = "account_id")
    @JsonIgnore
    private Account account;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "previous_mandate_id")
    @JsonIgnore
    private Mandate previousMandate;

    @OneToMany(mappedBy = "mandate")
    private List<Payment> payments = new ArrayList<>();

    @Enumerated(EnumType.STRING)
    private MandateStatus status;

    @Enumerated(EnumType.STRING)
    private SequenceType sequenceType;

    public void setStatus(MandateStatus status) {
        if (DEACTIVE.equals(this.status)) {
            throw new IllegalMandateStatusException();
        }
        this.status = status;
    }

    public boolean hasPreviousMandate() {
        return previousMandate != null;
    }

    public void addPayment(Payment payment) {
        payments.add(payment);
    }

    @JsonIgnore
    public boolean isAmendmentEnabled() {
        return getAmendmentMandate() != null;
    }

    @JsonIgnore
    public Mandate getAmendmentMandate() {
        if (hasAtLeastOnePaymentSentToTheBank()) {
            return null;
        }
        return getPreviousMandateWithPaymentSentToTheBank();
    }

    private Mandate getPreviousMandateWithPaymentSentToTheBank() {
        Mandate current = this.previousMandate;
        while (current != null) {
            if (current.hasAtLeastOnePaymentSentToTheBank()) {
                return current;
            }
            current = current.previousMandate;
        }
        return null;
    }

    private boolean hasAtLeastOnePaymentSentToTheBank() {
        List<Payment> paymentsSentToBank = getAllPaymentsSentToBank();
        return paymentsSentToBank.size() > ZERO;
    }

    private List<Payment> getAllPaymentsSentToBank() {
        return payments.stream()
                .filter(payment -> payment.equalsStatus(PaymentStatus.SENT_TO_BANK))
                .collect(Collectors.toList());
    }

    public String getUmr() {
        return "GENTLE_ART_" + getId();
    }

    @JsonIgnore
    public String getDateOfSignatureAsString() {
        return dateOfSignature.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

}