package eu.gentleart.sepa.payment.service.broker.payment;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class InstitutionIdentification {
    private Institution institution;

    public static InstitutionIdentification of(String bic) {
        Institution institution = new Institution();
        institution.setBic(bic);

        InstitutionIdentification institutionIdentification = new InstitutionIdentification();
        institutionIdentification.setInstitution(institution);

        return institutionIdentification;
    }

}
