package eu.gentleart.sepa.payment.service;

import eu.gentleart.sepa.payment.service.tenant.Tenant;
import eu.gentleart.sepa.payment.service.tenant.TenantSessionStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class PersistentController<T extends ResourceEntity> {

    private static final Logger logger = LoggerFactory.getLogger(PersistentController.class);

    public static final String HEADER_REQUEST_ID = "x-sepa-payment-request-id";
    public static final String REQUEST_ID_UNIQUE_KEY_SUFFIX_NAME = "unique_request_id";

    public abstract T save(T entityObject);

    public abstract ResourceEntity findByRequestId(String requestId);

    public ResponseEntity<T> persistOrGetEntityByRequestId(T persistentObject, HttpStatus statusCode) {
        try {
            ResourceEntity createdEntity = save(persistentObject);
            return new ResponseEntity(createdEntity, statusCode);
        } catch (DataIntegrityViolationException e) {
            return handleDuplicateRequestIdException(persistentObject, e);
        }
    }

    private ResponseEntity<T> handleDuplicateRequestIdException(ResourceEntity persistentObject, DataIntegrityViolationException e) {
        String errorMessage = getErrorMessage(e);

        if (isDuplicatedKeyRequestIdError(errorMessage)) {
            logger.warn("Duplicated requestId. Returning the existing entity. errorMessage: {}", errorMessage);
            ResourceEntity existingEntity = findByRequestId(persistentObject.getRequestId());
            return new ResponseEntity(existingEntity, HttpStatus.OK);
        }

        logger.warn("Unable to identify the database error. errorMessage: {}", errorMessage);
        throw e;
    }

    private boolean isDuplicatedKeyRequestIdError(String errorMessage) {
        return errorMessage.contains(REQUEST_ID_UNIQUE_KEY_SUFFIX_NAME);
    }

    private String getErrorMessage(DataIntegrityViolationException e) {
        StringBuilder message = new StringBuilder();
        message.append(e.getMessage());
        return message.toString();
    }

    protected Tenant getCurrentTenant() {
        return TenantSessionStore.getCurrentTenant();
    }

}