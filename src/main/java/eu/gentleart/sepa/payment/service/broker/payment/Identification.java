package eu.gentleart.sepa.payment.service.broker.payment;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Identification {
    private String endToEndId;

    public static Identification of(Object endToEndId) {
        Identification identification = new Identification();
        identification.setEndToEndId(String.valueOf(endToEndId));
        return identification;
    }
}
