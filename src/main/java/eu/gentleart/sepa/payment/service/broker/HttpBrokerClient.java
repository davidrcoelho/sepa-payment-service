package eu.gentleart.sepa.payment.service.broker;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class HttpBrokerClient {

    private static final Logger logger = LoggerFactory.getLogger(HttpBrokerClient.class);

    public String post(String url, String requestId, Object content) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        String request = mapper.writeValueAsString(content);

        logger.info("Sending post request. request: {}", request);

        return Request.Post(url)
                .addHeader("x-broker-request-id", requestId)
                .bodyString(request, ContentType.APPLICATION_JSON)
                .execute()
                .returnContent().asString();

    }

}
