package eu.gentleart.sepa.payment.service.payment.document;

public enum DocumentStatus {
    NEW,
    READY_TO_SEND,
    SENT_TO_BROKER
}
