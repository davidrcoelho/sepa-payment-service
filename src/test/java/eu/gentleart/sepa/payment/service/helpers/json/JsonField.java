package eu.gentleart.sepa.payment.service.helpers.json;


import com.jayway.jsonpath.DocumentContext;

public abstract class JsonField {
    private String name;

    public JsonField(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    abstract public void changeValue(DocumentContext documentContext);

}

