package eu.gentleart.sepa.payment.service.helpers.json;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

public class JsonParser {

    public static String changeValues(String json, JsonField... fields) {
        DocumentContext documentContext = JsonPath.parse(json);

        for (JsonField field : fields) {
            field.changeValue(documentContext);
        }

        return documentContext.jsonString();
    }
}
