package eu.gentleart.sepa.payment.service.helpers;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class UniqueNumberGenerator {

    private static Set<String> uuids = new HashSet<>();

    public synchronized static String newUUID() {

        String uuid = UUID.randomUUID().toString();
        if (uuids.contains(uuid)) {
            throw new RuntimeException("Duplicated UUID. It should never happen!");
        }

        uuids.add(uuid);
        return uuid;
    }


}
