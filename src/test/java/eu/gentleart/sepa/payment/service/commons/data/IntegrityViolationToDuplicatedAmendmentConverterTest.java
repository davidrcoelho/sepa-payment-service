package eu.gentleart.sepa.payment.service.commons.data;

import eu.gentleart.sepa.payment.service.api.exception.MandateAlreadyHasAmendmentException;
import org.junit.jupiter.api.Test;
import org.springframework.dao.DataIntegrityViolationException;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

class IntegrityViolationToDuplicatedAmendmentConverterTest {

    @Test
    public void should_throw_custom_exception_when_find_fragment_in_error_message() {

        DataIntegrityViolationException exception = new DataIntegrityViolationException("unique_previous_mandate_id violated");

        IntegrityViolationToDuplicatedAmendmentConverter.CustomCallable callable = mock(IntegrityViolationToDuplicatedAmendmentConverter.CustomCallable.class);
        doThrow(exception).when(callable).call();

        IntegrityViolationToDuplicatedAmendmentConverter checker = new IntegrityViolationToDuplicatedAmendmentConverter();

        assertThatThrownBy(() -> checker.execute(callable))
                .isInstanceOf(MandateAlreadyHasAmendmentException.class)
                .hasMessageContaining("Mandate already has amendment");

    }

    @Test
    public void should_throw_original_exception_when_fragment_is_not_found_in_error_message() {

        DataIntegrityViolationException exception = new DataIntegrityViolationException("Unknown integrity error");

        IntegrityViolationToDuplicatedAmendmentConverter.CustomCallable callable = mock(IntegrityViolationToDuplicatedAmendmentConverter.CustomCallable.class);
        doThrow(exception).when(callable).call();

        IntegrityViolationToDuplicatedAmendmentConverter checker = new IntegrityViolationToDuplicatedAmendmentConverter();

        assertThatThrownBy(() -> checker.execute(callable))
                .isInstanceOf(DataIntegrityViolationException.class)
                .hasMessageContaining("Unknown integrity error");
    }

}