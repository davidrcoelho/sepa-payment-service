package eu.gentleart.sepa.payment.service.helpers;

import eu.gentleart.sepa.payment.service.helpers.json.ChangeField;
import eu.gentleart.sepa.payment.service.helpers.json.JsonParser;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;

public class LocalFileReader {

    public static File getFileFromResources(String fileName) {

        ClassLoader classLoader = LocalFileReader.class.getClassLoader();

        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file is not found!");
        } else {
            return new File(resource.getFile());
        }

    }

    public static String readFile(String filename) throws IOException {
        File file = getFileFromResources(filename);
        return FileUtils.readFileToString(file, Charset.defaultCharset());
    }

    public static String readJsonFile(String filename, ChangeField... replacements) throws IOException {
        String json = LocalFileReader.readFile(filename);
        return JsonParser.changeValues(json, replacements);
    }

}

