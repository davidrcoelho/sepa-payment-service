package eu.gentleart.sepa.payment.service.helpers;

import eu.gentleart.sepa.payment.service.helpers.json.JsonField;
import eu.gentleart.sepa.payment.service.helpers.json.JsonParser;
import com.fasterxml.jackson.databind.module.SimpleModule;
import io.swagger.oas.inflector.examples.ExampleBuilder;
import io.swagger.oas.inflector.examples.models.Example;
import io.swagger.oas.inflector.processors.JsonNodeExampleSerializer;
import io.swagger.util.Json;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.parser.OpenAPIV3Parser;

import java.util.Map;

public class OpenApiParser {

    private OpenApiParser(){}

    public static String exampleOf(String definition) {
        OpenAPI swagger = new OpenAPIV3Parser().read("apidoc.yaml");
        Map<String, Schema> definitions = swagger.getComponents().getSchemas();
        Schema model = definitions.get(definition);
        Example example = ExampleBuilder.fromSchema(model, definitions);
        SimpleModule simpleModule = new SimpleModule().addSerializer(new JsonNodeExampleSerializer());
        Json.mapper().registerModule(simpleModule);
        return Json.pretty(example);
    }

    public static String exampleOf(String definition, JsonField... replacements) {
        String json = exampleOf(definition);
        return JsonParser.changeValues(json, replacements);
    }

}
