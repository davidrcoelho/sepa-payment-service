package eu.gentleart.sepa.payment.service.helpers;

import org.mockserver.matchers.Times;
import org.mockserver.model.Delay;
import org.springframework.http.HttpStatus;

import java.util.concurrent.TimeUnit;

public class HttpMockEndpoint {

    private String path;
    private String method;
    private Times times;

    private int responseStatusCode;
    private Delay responseDelay;

    public HttpMockEndpoint withPath(String path) {
        this.path = path;
        return this;
    }

    public HttpMockEndpoint withMethod(String method) {
        this.method = method;
        return this;
    }

    public HttpMockEndpoint withTimes(Times times) {
        this.times = times;
        return this;
    }

    public HttpMockEndpoint withResponseStatusCode(HttpStatus httpStatus) {
        this.responseStatusCode = httpStatus.value();
        return this;
    }

    public HttpMockEndpoint withResponseDelayInSeconds(int delayInSeconds) {
        this.responseDelay = new Delay(TimeUnit.SECONDS, delayInSeconds);
        return this;
    }

    public String getPath() {
        return path;
    }

    public String getMethod() {
        return method;
    }

    public Times getTimes() {
        return times;
    }

    public int getResponseStatusCode() {
        return responseStatusCode;
    }

    public Delay getResponseDelay() {
        return responseDelay;
    }
}
