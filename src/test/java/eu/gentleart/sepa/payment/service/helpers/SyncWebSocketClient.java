package eu.gentleart.sepa.payment.service.helpers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.*;
import java.io.IOException;
import java.net.URI;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@ClientEndpoint
public class SyncWebSocketClient {

    private static final Logger logger = LoggerFactory.getLogger(SyncWebSocketClient.class);

    private final String label;
    private final long timeout;

    private Session session;
    private BlockingQueue<String> messages = new LinkedBlockingQueue<>();

    public SyncWebSocketClient(String label, String url, long timeout) throws IOException, DeploymentException {
        this.label = label;
        this.timeout = timeout;
        session = connectToServer(url);
    }

    private Session connectToServer(String url) throws IOException, DeploymentException {
        logger.info("Connecting to the server: {} - {}...", label, url);
        WebSocketContainer container = ContainerProvider.getWebSocketContainer();
        return container.connectToServer(this, URI.create(url));
    }

    public void send(String message) {
        logger.info("Sending message: {} - {}", label, message.replace("\\n", ""));
        session.getAsyncRemote().sendText(message);
    }

    public String nextMessage() throws InterruptedException, TimeoutException {
        logger.info("Waiting for the next message, timeoutInMillis: {} - {}...", label, timeout);
        String receivedMessage = messages.poll(timeout, TimeUnit.MILLISECONDS);

        if (receivedMessage == null) {
            logger.error("No message received. Timeout.");
            throw new TimeoutException("Timeout waiting for messages on websocket");
        }

        return receivedMessage;
    }

    @OnMessage
    public void onMessage(String message) {
        logger.info("onMessage: {} - {}", label, message);
        messages.add(message);
    }

    @OnOpen
    public void onOpen(Session session) {
        logger.info("onOpen: " + session);
    }

    @OnClose
    public void onClose(Session session) {
        logger.info("onOpen: " + session);
    }

    public void close() throws IOException {
        session.close();
    }
}