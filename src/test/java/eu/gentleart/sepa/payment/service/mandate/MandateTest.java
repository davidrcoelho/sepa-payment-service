package eu.gentleart.sepa.payment.service.mandate;

import eu.gentleart.sepa.payment.service.payment.Payment;
import eu.gentleart.sepa.payment.service.payment.PaymentStatus;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

class MandateTest {

    @Test
    void given_a_mandate_with_no_payment_should_have_no_amendment() {
        Mandate m1 = new Mandate();
        assertThat(m1.isAmendmentEnabled()).isFalse();
        assertThat(m1.getAmendmentMandate()).isNull();
    }

    @Test
    void given_a_mandate_with_a_previous_payment_but_no_previous_mandate_should_have_no_amendment() {

        Mandate m1 = new Mandate();
        m1.addPayment(payment(PaymentStatus.SENT_TO_BANK));
        m1.addPayment(payment(PaymentStatus.SENT_TO_BANK));
        m1.addPayment(payment(PaymentStatus.SENT_TO_BANK));

        assertThat(m1.isAmendmentEnabled()).isFalse();
        assertThat(m1.getAmendmentMandate()).isNull();
    }

    @Test
    void given_a_mandate_with_no_payment_sent_to_the_bank_AND_with_previous_mandate_that_has_payment_sent_to_the_bank_should_return_amendment_TRUE() {

        Mandate m1 = new Mandate();
        m1.addPayment(payment(PaymentStatus.CREATED));
        m1.addPayment(payment(PaymentStatus.SENT_TO_BANK));

        Mandate m2 = new Mandate();
        m2.addPayment(payment(PaymentStatus.CREATED));
        m2.addPayment(payment(PaymentStatus.CREATED));
        m2.addPayment(payment(PaymentStatus.CREATED));
        m2.setPreviousMandate(m1);

        assertThat(m1.isAmendmentEnabled()).isFalse();
        assertThat(m1.getAmendmentMandate()).isNull();

        assertThat(m2.isAmendmentEnabled()).isTrue();
        assertThat(m2.getAmendmentMandate()).isEqualTo(m1);
    }

    @Test
    void given_a_mandate_with_no_payment_sent_to_the_bank_AND_with_multiple_previous_mandate_that_has_payment_sent_to_the_bank_should_return_amendment_TRUE() {
        Mandate m1 = new Mandate();
        m1.addPayment(payment(PaymentStatus.SENT_TO_BANK));

        Mandate m2 = new Mandate();
        m2.addPayment(payment(PaymentStatus.CREATED));
        m2.addPayment(payment(PaymentStatus.CREATED));
        m2.addPayment(payment(PaymentStatus.CREATED));
        m2.setPreviousMandate(m1);

        Mandate m3 = new Mandate();
        m3.addPayment(payment(PaymentStatus.CREATED));
        m3.setPreviousMandate(m2);

        assertThat(m1.isAmendmentEnabled()).isFalse();
        assertThat(m1.getAmendmentMandate()).isNull();

        assertThat(m2.isAmendmentEnabled()).isTrue();
        assertThat(m2.getAmendmentMandate()).isEqualTo(m1);

        assertThat(m3.isAmendmentEnabled()).isTrue();
        assertThat(m3.getAmendmentMandate()).isEqualTo(m1);
    }

    @Test
    void given_a_mandate_with_one_payment_sent_to_the_bank_AND_with_previous_mandate_that_has_payment_sent_to_the_bank_should_return_amendment_FALSE() {
        Mandate m1 = new Mandate();
        m1.addPayment(payment(PaymentStatus.SENT_TO_BANK));

        Mandate m2 = new Mandate();
        m2.addPayment(payment(PaymentStatus.SENT_TO_BANK));
        m2.setPreviousMandate(m1);

        assertThat(m2.isAmendmentEnabled()).isFalse();
        assertThat(m2.getAmendmentMandate()).isNull();
    }

    @Test
    void given_a_mandate_with_status_DEACTIVE_should_not_be_permitted_change_it_to_ACTIVE() {
        Mandate m1 = new Mandate();
        m1.setStatus(MandateStatus.ACTIVE);
        m1.setStatus(MandateStatus.DEACTIVE);
        assertThatThrownBy(() -> m1.setStatus(MandateStatus.ACTIVE))
                .isInstanceOf(IllegalMandateStatusException.class)
                .hasMessageContaining("Cannot change the mandate status");
    }

    private Payment payment(PaymentStatus status) {
        Payment payment = new Payment();
        payment.setStatus(status);
        return payment;
    }

}