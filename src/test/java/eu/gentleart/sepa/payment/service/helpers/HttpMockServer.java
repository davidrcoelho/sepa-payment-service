package eu.gentleart.sepa.payment.service.helpers;

import org.json.JSONException;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.mockserver.verify.VerificationTimes;
import org.skyscreamer.jsonassert.Customization;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.comparator.CustomComparator;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;

import static org.mockserver.model.HttpRequest.request;

public class HttpMockServer {

    private ClientAndServer clientAndServer;

    private int port;
    private HttpMockEndpoint endpoint;

    public HttpMockServer(int port) {
        this.port = port;
    }

    public HttpMockServer() throws IOException {
        this.port = findAvailablePort();
    }

    public HttpMockServer withEndpoint(HttpMockEndpoint endpoint) {
        this.endpoint = endpoint;
        return this;
    }

    public HttpMockServer start() throws IOException {
        clientAndServer = ClientAndServer.startClientAndServer(port);
        clientAndServer.when(
                request()
                        .withMethod(endpoint.getMethod())
                        .withPath(endpoint.getPath()),
                endpoint.getTimes())
                .respond(
                        HttpResponse.response()
                                .withStatusCode(endpoint.getResponseStatusCode())
                                .withDelay(endpoint.getResponseDelay())
                );
        return this;
    }

    public Object getCallbackUrl() {
        return String.format("http://localhost:%s", getPort()) + endpoint.getPath();
    }

    public void verifyBodyRequest(VerificationTimes times, String bodyRequest, String... ignoredFields) throws JSONException {

        clientAndServer.verify(
                request()
                        .withMethod(endpoint.getMethod())
                        .withPath(endpoint.getPath()),
                times);

        HttpRequest[] recordedRequests = clientAndServer.retrieveRecordedRequests(
                request()
                        .withMethod(endpoint.getMethod())
                        .withPath(endpoint.getPath())
        );

        for (HttpRequest request : recordedRequests) {
            String body = request.getBodyAsString();

            Customization[] ignoreFieldValueCustomization = getIgnoreFieldValueCustomizations(ignoredFields);
            CustomComparator comparator = new CustomComparator(JSONCompareMode.STRICT, ignoreFieldValueCustomization);
            JSONAssert.assertEquals(bodyRequest, body, comparator);
        }
    }

    private Customization[] getIgnoreFieldValueCustomizations(String... ignoredFields) {
        List<Customization> customizations = new ArrayList<>();
        for (String field : ignoredFields) {
            Customization ignoreFieldValue = new Customization(field, (expected, actual) -> true);
            customizations.add(ignoreFieldValue);
        }
        return customizations.toArray(new Customization[ignoredFields.length]);
    }

    public int getPort() {
        return port;
    }

    private static int findAvailablePort() throws IOException {
        try (ServerSocket socket = new ServerSocket(0)) {
            return socket.getLocalPort();
        }
    }

    public void stop() {
        clientAndServer.stop();
    }
}

