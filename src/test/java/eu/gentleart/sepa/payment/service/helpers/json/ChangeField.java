package eu.gentleart.sepa.payment.service.helpers.json;

import com.jayway.jsonpath.DocumentContext;

public class ChangeField extends JsonField {
    private Object value;

    public ChangeField(String name, Object value) {
        super(name);
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public void changeValue(DocumentContext documentContext) {
        documentContext.set(getName(), value);
    }

}
