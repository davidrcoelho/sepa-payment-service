package eu.gentleart.sepa.payment.service.helpers.json;

import com.jayway.jsonpath.DocumentContext;

public class DeleteField extends JsonField {

    public DeleteField(String name) {
        super(name);
    }

    @Override
    public void changeValue(DocumentContext documentContext) {
        documentContext.delete(getName());
    }

}
