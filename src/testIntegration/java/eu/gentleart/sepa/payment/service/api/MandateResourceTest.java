package eu.gentleart.sepa.payment.service.api;

import eu.gentleart.sepa.payment.service.AbstractApiTest;
import eu.gentleart.sepa.payment.service.helpers.OpenApiParser;
import eu.gentleart.sepa.payment.service.helpers.UniqueNumberGenerator;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;

import static eu.gentleart.sepa.payment.service.PersistentController.HEADER_REQUEST_ID;
import static eu.gentleart.sepa.payment.service.api.AccountResourceTest.createAccount;
import static eu.gentleart.sepa.payment.service.helpers.UniqueNumberGenerator.newUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

public class MandateResourceTest extends AbstractApiTest {

    @Test
    public void given_a_post_request_should_create_new_mandate() {
        int accountId = AccountResourceTest.createAccount()
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");

        int mandateId =
            createMandate(accountId)
                    .statusCode(HttpStatus.SC_CREATED)
                    .extract().path("id");

        getMandate(accountId, mandateId)
                .statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void given_a_post_request_to_an_account_that_already_has_a_mandate_should_return_httpError400() {
        int accountId = AccountResourceTest.createAccount()
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");

        createMandate(accountId)
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");

        createMandate(accountId)
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("message", equalTo("Account already has a mandate"));
    }

    @Test
    public void given_a_get_request_to_a_non_existing_mandateId_should_return_404_error() {

        int accountId = AccountResourceTest.createAccount()
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");

        int nonExistingMandateId = 123456789;

        getMandate(accountId, nonExistingMandateId)
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .body("message", containsString("Resource not found"))
                .body("description", containsString(String.valueOf(nonExistingMandateId)));

    }

    @Test
    public void given_a_duplicated_request_should_return_the_same_mandate_created_previously() {
        String requestId = newUUID();

        int accountId = AccountResourceTest.createAccount()
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");

        int firstRequestMandateId = createMandate(accountId, requestId)
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");

        int secondRequestMandateId = createMandate(accountId, requestId)
                .statusCode(HttpStatus.SC_OK)
                .extract().path("id");

        assertThat(firstRequestMandateId).isEqualTo(secondRequestMandateId);
    }

    static ValidatableResponse createMandate(int accountIdPathParam) {
        String mandateRequestBody = OpenApiParser.exampleOf("MandateRequest");
        return createMandate(newUUID(), mandateRequestBody, accountIdPathParam);
    }

    static ValidatableResponse createMandate(String requestUser, int accountIdPathParam) {
        String mandateRequestBody = OpenApiParser.exampleOf("MandateRequest");
        return
            given(requestUser)
                    .contentType(ContentType.JSON)
                    .header(HEADER_REQUEST_ID, newUUID())
                    .body(mandateRequestBody)
                    .post("/v1/accounts/{id}/mandates", accountIdPathParam)
            .then()
                    .log().all();
    }

    static ValidatableResponse createMandate(int accountIdPathParam, String requestId) {
        String mandateRequestBody = OpenApiParser.exampleOf("MandateRequest");
        return createMandate(requestId, mandateRequestBody, accountIdPathParam);
    }

    static ValidatableResponse createMandate(String requestId, String mandateRequestBody, int accountIdPathParam) {
        return post("/v1/accounts/{id}/mandates", requestId, mandateRequestBody, accountIdPathParam);
    }

    static ValidatableResponse getMandate(int accountId, int mandateId) {
        return given()
                .get("/v1/accounts/{id}/mandates/{mandateId}", accountId, mandateId)
        .then()
                .log().all();
    }

    static ValidatableResponse getMandate(String requestUser, int accountId, int mandateId) {
        return given(requestUser)
                    .get("/v1/accounts/{id}/mandates/{mandateId}", accountId, mandateId)
                .then()
                    .log().all();
    }

}
