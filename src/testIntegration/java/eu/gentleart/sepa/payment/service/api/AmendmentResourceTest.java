package eu.gentleart.sepa.payment.service.api;

import eu.gentleart.sepa.payment.service.AbstractApiTest;
import eu.gentleart.sepa.payment.service.helpers.OpenApiParser;
import eu.gentleart.sepa.payment.service.helpers.json.ChangeField;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;

import static eu.gentleart.sepa.payment.service.api.AccountResourceTest.createAccount;
import static eu.gentleart.sepa.payment.service.api.MandateResourceTest.createMandate;
import static eu.gentleart.sepa.payment.service.api.MandateResourceTest.getMandate;
import static eu.gentleart.sepa.payment.service.helpers.UniqueNumberGenerator.newUUID;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

public class AmendmentResourceTest extends AbstractApiTest {

    @Test
    public void given_a_post_request_should_create_new_amendment() {
        int accountId = createAccount()
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");

        int mandateId =
            createMandate(accountId)
                    .statusCode(HttpStatus.SC_CREATED)
                    .extract().path("id");

        String amendmentRequestBody = OpenApiParser.exampleOf("MandateRequest",
                new ChangeField("name", "Helio Gracie"));

        int newMandateId =
            createAmendment(amendmentRequestBody, accountId, mandateId)
                    .statusCode(HttpStatus.SC_CREATED)
                    .extract().path("id");

        getMandate(accountId, mandateId)
                .statusCode(HttpStatus.SC_OK);

        getMandate(accountId, newMandateId)
                .statusCode(HttpStatus.SC_OK)
                .body("name", equalTo("Helio Gracie"));
    }

    @Test
    public void given_a_two_amendment_request_to_the_same_mandate_should_return_httpError400() {
        int accountId = createAccount()
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");

        int mandateId =
                createMandate(accountId)
                        .statusCode(HttpStatus.SC_CREATED)
                        .extract().path("id");

        createAmendment(accountId, mandateId)
                .statusCode(HttpStatus.SC_CREATED);

        createAmendment(accountId, mandateId)
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("message", containsString("Mandate already has amendment"));
    }

    @Test
    public void given_a_multiple_amendment_should_create_list_of_mandates() {
        int accountId = createAccount()
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");

        int mandateId = createMandate(accountId)
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");

        int firstAmendmentMandateId = createAmendment(accountId, mandateId)
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");

        int secondAmendmentMandateId = createAmendment(accountId, firstAmendmentMandateId)
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");

        int thirdAmendmentMandateId = createAmendment(accountId, secondAmendmentMandateId)
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");

        getAmendments(accountId, thirdAmendmentMandateId)
                .statusCode(HttpStatus.SC_OK)
                .body("root.id", equalTo(thirdAmendmentMandateId))
                .body("previousMandates[0].id", equalTo(secondAmendmentMandateId))
                .body("previousMandates[1].id", equalTo(firstAmendmentMandateId))
                .body("previousMandates[2].id", equalTo(mandateId));

    }

    static ValidatableResponse createAmendment(int accountIdPathParam, int mandateIdPathParam) {
        String requestBody = OpenApiParser.exampleOf("MandateRequest");
        return createAmendment(newUUID(), requestBody, accountIdPathParam, mandateIdPathParam);
    }

    static ValidatableResponse createAmendment(String requestBody, int accountIdPathParam, int mandateIdPathParam) {
        return createAmendment(newUUID(), requestBody, accountIdPathParam, mandateIdPathParam);
    }

    static ValidatableResponse createAmendment(String requestId, String mandateRequestBody, Object... pathParams) {
        return post("/v1/accounts/{id}/mandates/{mandateId}/amendments", requestId, mandateRequestBody, pathParams);
    }

    static ValidatableResponse getAmendments(int accountId, int mandateId) {
        return
                given()
                        .contentType(ContentType.JSON)
                        .get("/v1/accounts/{id}/mandates/{mandateId}/amendments", accountId, mandateId)
                .then()
                        .log().all();
    }

}
