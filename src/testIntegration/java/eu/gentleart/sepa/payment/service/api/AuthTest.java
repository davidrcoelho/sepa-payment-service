package eu.gentleart.sepa.payment.service.api;

import eu.gentleart.sepa.payment.service.AbstractApiTest;
import eu.gentleart.sepa.payment.service.helpers.OpenApiParser;
import eu.gentleart.sepa.payment.service.helpers.json.ChangeField;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;

import static eu.gentleart.sepa.payment.service.PersistentController.HEADER_REQUEST_ID;
import static eu.gentleart.sepa.payment.service.helpers.UniqueNumberGenerator.newUUID;

public class AuthTest extends AbstractApiTest {

    @Test
    public void given_a_non_existing_user_should_return_httpError403() {
        given("invalidUser")
                .contentType(ContentType.JSON)
                .get("/v1/accounts/1")
        .then()
                .statusCode(HttpStatus.SC_UNAUTHORIZED)
                .log().all();
    }

    @Test
    public void given_an_account_created_in_a_tenant_should_not_be_visible_in_other_tenant() {

        String accountRequest = OpenApiParser.exampleOf("AccountRequest",
                new ChangeField("clientId", newUUID()));

        int accountIdForTenantTest1 =
            given(TENANT_NAME_FOR_TESTING1)
                    .contentType(ContentType.JSON)
                    .header(HEADER_REQUEST_ID, newUUID())
                    .body(accountRequest)
                    .post("/v1/accounts")
            .then()
                    .log().all()
                    .statusCode(HttpStatus.SC_CREATED)
                    .extract().path("id");

        given(TENANT_NAME_FOR_TESTING1)
                .get("/v1/accounts/{id}", accountIdForTenantTest1)
        .then()
                .statusCode(HttpStatus.SC_OK)
                .log().all();

        given(TENANT_NAME_FOR_TESTING2)
                .get("/v1/accounts/{id}", accountIdForTenantTest1)
        .then()
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .log().all();

    }


}
