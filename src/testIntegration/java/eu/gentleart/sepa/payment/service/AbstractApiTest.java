package eu.gentleart.sepa.payment.service;

import com.atlassian.oai.validator.restassured.OpenApiValidationFilter;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.testcontainers.containers.DockerComposeContainer;
import org.testcontainers.containers.PostgreSQLContainer;

import java.io.File;

import static eu.gentleart.sepa.payment.service.PersistentController.HEADER_REQUEST_ID;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = {"local", "test"})
@ContextConfiguration(initializers = {AbstractApiTest.PropertiesInitializer.class})
public abstract class AbstractApiTest {

    public static final String TENANT_NAME_FOR_TESTING1 = "test1";
    public static final String TENANT_NAME_FOR_TESTING2 = "test2";

    public static final String MOCK_HEADER_USER_VALIDATOR = "Authorization";

    public static final String SEPA_BROKER_SERVICE_NAME = "sepa-broker-api";
    public static final int SEPA_BROKER_SERVICE_PORT = 8080;

    @LocalServerPort
    private int port;

    protected static PostgreSQLContainer postgres = new PostgreSQLContainer<>("postgres:10.11")
            .withUsername("postgres")
            .withPassword("test")
            .withDatabaseName("postgres")
            .withExposedPorts(5432);

    protected static DockerComposeContainer SEPA_BROKER_API = new DockerComposeContainer(new File("src/testIntegration/resources/docker-compose-broker-test.yaml"))
            .withExposedService(SEPA_BROKER_SERVICE_NAME, SEPA_BROKER_SERVICE_PORT);

    static {
        postgres.start();
        SEPA_BROKER_API.start();
    }


    @BeforeEach
    public void beforeEach() {
        RestAssured.port = port;
    }

    protected static String getSepaBrokerUrl() {
        int sepaBrokerMappedPort = SEPA_BROKER_API.getServicePort(SEPA_BROKER_SERVICE_NAME, SEPA_BROKER_SERVICE_PORT);
        return "http://localhost:" + sepaBrokerMappedPort;
    }

    protected static String getSepaBrokerDocumentsUrl() {
        return getSepaBrokerUrl() + "/v1/direct-debit/documents";
    }

    protected static RequestSpecification getSepaBrokerRequestSpec() {
        return new RequestSpecBuilder().setBaseUri(getSepaBrokerUrl()).build();
    }

    public static RequestSpecification given() {
        return given(TENANT_NAME_FOR_TESTING1);
    }

    public static RequestSpecification given(String authenticatedUser) {

        return RestAssured.given()
                .log().all()
                .header(MOCK_HEADER_USER_VALIDATOR, authenticatedUser)
                ;//.filter(new OpenApiValidationFilter("apidoc.yaml"));
    }

    protected static ValidatableResponse post(String url, String requestId, String body, Object... pathParams) {
        return
                given()
                        .contentType(ContentType.JSON)
                        .header(HEADER_REQUEST_ID, requestId)
                        .body(body)
                .post(url, pathParams)
                        .then()
                        .log().all();
    }

    protected static ValidatableResponse put(String url, String requestId, String body, Object... pathParams) {
        return
                given()
                        .contentType(ContentType.JSON)
                        .header(HEADER_REQUEST_ID, requestId)
                        .body(body)
                .put(url, pathParams)
                        .then()
                        .log().all();
    }

    public static class PropertiesInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertySourceUtils.addInlinedPropertiesToEnvironment(configurableApplicationContext,
                    "spring.datasource.url=jdbc:postgresql://127.0.0.1:" + postgres.getMappedPort(5432) + "/postgres",
                    "sepa.broker.documents.url=" + getSepaBrokerDocumentsUrl()
            );
        }
    }

}
