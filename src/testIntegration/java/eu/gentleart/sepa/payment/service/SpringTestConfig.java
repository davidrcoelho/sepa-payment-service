package eu.gentleart.sepa.payment.service;

import eu.gentleart.sepa.payment.service.commons.spring.TenantAuthValidator;
import eu.gentleart.sepa.payment.service.commons.spring.TenantAuthValidatorConfig;
import eu.gentleart.sepa.payment.service.tenant.Tenant;
import eu.gentleart.sepa.payment.service.tenant.TenantNotFoundException;
import eu.gentleart.sepa.payment.service.tenant.TenantRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Optional;

@Configuration
@Import(TenantAuthValidatorConfig.class)
public class SpringTestConfig {

    @Autowired
    private TenantRepository tenantRepository;

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setMessageConverters(Arrays.asList(new MappingJackson2HttpMessageConverter()));
        return restTemplate;
    }

    @Bean
    @Primary
    public TenantAuthValidator mockTenantAuthValidator() {
        return new MockTenantAuthValidator(tenantRepository);
    }


}

class MockTenantAuthValidator extends TenantAuthValidator {

    private static final Logger logger = LoggerFactory.getLogger(MockTenantAuthValidator.class);

    public MockTenantAuthValidator(TenantRepository repository) {
        super(repository);
    }

    @Override
    public Tenant validate(HttpServletRequest request) {

        String username = request.getHeader(AbstractApiTest.MOCK_HEADER_USER_VALIDATOR);
        Optional<Tenant> tenantTest = repository.findByUsername(username);

        //logger.warn(" :::: MOCK - AUTH VALIDATOR - returning user for testing. Authentication: {}, Tenant: {}", username, tenantTest);
        if (tenantTest.isPresent()) {
            return tenantTest.get();
        }

        logger.warn(" :::: MOCK - AUTH VALIDATOR - TENANT NOT FOUND");
        throw new TenantNotFoundException();

    }

}