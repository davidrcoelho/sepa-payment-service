package eu.gentleart.sepa.payment.service.api;

import eu.gentleart.sepa.payment.service.AbstractApiTest;
import eu.gentleart.sepa.payment.service.helpers.OpenApiParser;
import eu.gentleart.sepa.payment.service.helpers.json.ChangeField;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;

import static eu.gentleart.sepa.payment.service.PersistentController.HEADER_REQUEST_ID;
import static eu.gentleart.sepa.payment.service.helpers.UniqueNumberGenerator.newUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;

public class AccountResourceTest extends AbstractApiTest {

    @Test
    public void given_a_post_request_should_create_new_account() {
        createAccount()
                .statusCode(HttpStatus.SC_CREATED);
    }

    @Test
    public void given_a_put_request_should_update_existing_account() {
        String modifiedAccountIdentifier = "account-clientId-changed";
        String updateAccountRequest = OpenApiParser.exampleOf("AccountRequest",
                new ChangeField("clientId", modifiedAccountIdentifier));

        int accountId = createAccount()
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");

        updateAccount(updateAccountRequest, accountId)
                .statusCode(HttpStatus.SC_OK);

        getAccount(accountId)
                .statusCode(HttpStatus.SC_OK)
                .body("clientId", containsString(modifiedAccountIdentifier));
    }

    @Test
    public void given_a_duplicated_request_should_return_the_same_account_created_previously() {
        String requestId = newUUID();

        int firstRequestAccountId = createAccount(requestId)
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");

        int secondRequestAccountId = createAccount(requestId)
                .statusCode(HttpStatus.SC_OK)
                .extract().path("id");

        assertThat(firstRequestAccountId).isEqualTo(secondRequestAccountId);
    }

    @Test
    public void given_a_put_request_to_a_non_existing_accountId_should_return_404_error() {

        String updateAccountRequest = OpenApiParser.exampleOf("AccountRequest");

        int nonExistingAccountId = 1234;

        updateAccount(updateAccountRequest, nonExistingAccountId)
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .body("message", containsString("Resource not found"))
                .body("description", containsString(String.valueOf(nonExistingAccountId)));

    }


    static ValidatableResponse createAccount() {
        String accountRequest = OpenApiParser.exampleOf("AccountRequest",
                new ChangeField("clientId", newUUID()));
        return createAccount(accountRequest, newUUID());
    }

    static ValidatableResponse createAccount(String requestId) {
        String accountRequestBody = OpenApiParser.exampleOf("AccountRequest",
                new ChangeField("clientId", newUUID()));
        return createAccount(accountRequestBody, requestId);
    }

    static ValidatableResponse createAccount(String accountRequestBody, String requestId) {
        return post("/v1/accounts", requestId, accountRequestBody);
    }

    static ValidatableResponse createAccount(String requestUser, String accountRequestBody, String requestId) {
        return
            given(requestUser)
                    .contentType(ContentType.JSON)
                    .header(HEADER_REQUEST_ID, requestId)
                    .body(accountRequestBody)
                    .post("/v1/accounts")
            .then()
                    .log().all();
    }

    static ValidatableResponse updateAccount(String accountRequestBody, int accountIdPathParam) {
        return put("/v1/accounts/{id}", newUUID(), accountRequestBody, accountIdPathParam);
    }

    static ValidatableResponse getAccount(int accountId) {
        return given()
                .contentType(ContentType.JSON)
                .get("/v1/accounts/{id}", accountId)
        .then()
                .log().all();
    }


}
