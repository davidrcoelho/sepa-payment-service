package eu.gentleart.sepa.payment.service;

import eu.gentleart.sepa.payment.service.creditor.Creditor;
import eu.gentleart.sepa.payment.service.creditor.CreditorOptimisticLocker;
import eu.gentleart.sepa.payment.service.creditor.CreditorRepository;
import eu.gentleart.sepa.payment.service.tenant.Tenant;
import eu.gentleart.sepa.payment.service.tenant.TenantRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

public class CreditorLockTest extends AbstractApiTest {

    @Autowired
    private TenantRepository tenantRepository;

    @Autowired
    private CreditorRepository creditorRepository;

    @Autowired
    private CreditorOptimisticLocker creditorOptimisticLocker;

    @Test
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    void should_avoid_two_process_lock_the_same_creditor() {

        Tenant tenant1 = tenantRepository.findById(1).get();
        Tenant tenant2 = tenantRepository.findById(2).get();

        Creditor creditor1 = creditorRepository.findByTenantAndId(tenant1, 1).get();
        Creditor creditor2 = creditorRepository.findByTenantAndId(tenant2, 2).get();
        Creditor creditor3SameAs1 = creditorRepository.findByTenantAndId(tenant1, 1).get();

        assertThat(creditorOptimisticLocker.quietlyLock(creditor1)).isTrue();
        assertThat(creditorOptimisticLocker.quietlyLock(creditor2)).isTrue();
        assertThat(creditorOptimisticLocker.quietlyLock(creditor3SameAs1)).isFalse();

    }

}
