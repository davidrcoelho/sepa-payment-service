package eu.gentleart.sepa.payment.service.api;

import eu.gentleart.sepa.payment.service.AbstractApiTest;
import eu.gentleart.sepa.payment.service.helpers.OpenApiParser;
import eu.gentleart.sepa.payment.service.helpers.json.ChangeField;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.ExtractableResponse;
import io.restassured.response.Response;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import static eu.gentleart.sepa.payment.service.api.MandateResourceTest.createMandate;
import static eu.gentleart.sepa.payment.service.api.MandateResourceTest.getMandate;
import static eu.gentleart.sepa.payment.service.api.PaymentResourceTest.createPayment;
import static eu.gentleart.sepa.payment.service.helpers.UniqueNumberGenerator.newUUID;
import static org.awaitility.Awaitility.await;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasKey;

public class DocumentJobTest extends AbstractApiTest {

    @Value("${sepa.payments.jobs.pain08.first.scheduledDaysBeforeCollectionDate.default}")
    private int defaultScheduledDaysBeforeCollectionDate;

    //@Test
    void given_a_big_list_of_payments_should_create_a_document_to_send_to_the_bank() throws IOException {

        for (int i = 0; i < 205; i++) {
            AccountResponse A1 = createAccount();

            createMandate(A1.getId())
                    .statusCode(HttpStatus.SC_CREATED)
                    .extract().path("id");

            createPayments(A1);
        }

        AccountResponse A = createAccount();
        int M = createMandate(A.getId())
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");
        AccountMandatePair A1M1 = new AccountMandatePair(A, M);
        createPayments(A);

        await().atMost(6, TimeUnit.SECONDS)
                .pollInterval(2, TimeUnit.SECONDS)
                .untilAsserted(() -> mandatesHasBeenAddedToDocument(A1M1));

        System.out.println(">>>>>>>>");
        String pain08XmlContent = getSepaBrokerPain08File();
        String filename = "pain08-test.xml";
        FileUtils.writeStringToFile(new File(filename), pain08XmlContent, Charset.defaultCharset());
        System.out.println("<<<<<<<<");
    }

    @Test
    void given_a_list_of_payments_should_create_a_document_to_send_to_the_bank() {

        AccountResponse A1 = createAccount();

        AccountResponse A2 = createAccount();

        int M1 = createMandate(A1.getId())
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");

        int M2 = createMandate(A2.getId())
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");

        AccountMandatePair A1M1 = new AccountMandatePair(A1, M1);
        AccountMandatePair A2M2 = new AccountMandatePair(A2, M2);

        createPayments(A1, A1);
        createPayments(A2, A2, A2);

        getMandate(A1.getId(), M1)
                .statusCode(HttpStatus.SC_OK)
                .body("payments.size", equalTo(2));

        getMandate(A2.getId(), M2)
                .statusCode(HttpStatus.SC_OK)
                .body("payments.size", equalTo(3));

        try {
            await().atMost(6, TimeUnit.SECONDS)
                    .pollInterval(2, TimeUnit.SECONDS)
                    .untilAsserted(() -> mandatesHasBeenAddedToDocument(A1M1, A2M2));
        } catch (Exception e) {

        }

        System.out.println(">>>>>>>>");
        getSepaBrokerPain08File();
        System.out.println("<<<<<<<<");

        getMandate(A1.getId(), M1)
                .statusCode(HttpStatus.SC_OK)
                .extract().path("document.id");
    }

    static String getSepaBrokerPain08File() {
        return RestAssured.given(getSepaBrokerRequestSpec())
                .log().all()
                .contentType(ContentType.JSON)
                .get("/v1/direct-debit/documents/1/pain")
        .then()
                .log().all()
                .extract().asString();
    }

    private void mandatesHasBeenAddedToDocument(AccountMandatePair ... pairs) {
        for (AccountMandatePair pair : pairs) {
            System.out.println("---------------------------------");
            int accountId = pair.getAccountResponse().getId();
            int mandateId = pair.getMandateId();
            RestAssured.given()
                    .log().all()
                    .header(MOCK_HEADER_USER_VALIDATOR, TENANT_NAME_FOR_TESTING1)
                    .get("/v1/accounts/{id}/mandates/{mandateId}", accountId, mandateId)
            .then()
                    .log().all()
                    .body("payments[0].document", hasKey("brokerDocumentId"))
                    .statusCode(HttpStatus.SC_OK);
        }
    }

    void createPayments(AccountResponse ... accounts) {
        for (AccountResponse account : accounts) {
            String bodyRequest = buildPaymentRequest(account.getClientId());
            createPayment(bodyRequest)
                    .statusCode(HttpStatus.SC_OK);
        }
    }

    String buildPaymentRequest(String clientAccountId) {

        String clientPaymentId = newUUID();

        return OpenApiParser.exampleOf("PaymentBatchRequest",
                new ChangeField("$[0].clientAccountId", clientAccountId),
                new ChangeField("$[0].clientPaymentId", clientPaymentId),
                new ChangeField("$[0].requestedCollectionDate", getRequestedCollectionDate()),
                new ChangeField("$[0].amount.value", 5.00));
    }

    String getRequestedCollectionDate() {
        ZonedDateTime today = ZonedDateTime.now(ZoneId.of("+3"));
        ZonedDateTime requestedCollectionDate = today.plusDays(defaultScheduledDaysBeforeCollectionDate);
        return requestedCollectionDate.format(DateTimeFormatter.ISO_DATE_TIME);
    }

    private AccountResponse createAccount() {
        ExtractableResponse<Response> extractableResponse = AccountResourceTest.createAccount()
                .statusCode(HttpStatus.SC_CREATED)
                .extract();
        return new AccountResponse(extractableResponse);
    }

}

class AccountMandatePair {
    private AccountResponse accountResponse;
    private int mandateId;

    public AccountMandatePair(AccountResponse accountResponse, int mandateId) {
        this.accountResponse = accountResponse;
        this.mandateId = mandateId;
    }

    public AccountResponse getAccountResponse() {
        return accountResponse;
    }

    public int getMandateId() {
        return mandateId;
    }
}

class AccountResponse {

    private ExtractableResponse data;

    public AccountResponse(ExtractableResponse data) {
        this.data = data;
    }

    public int getId() {
        return data.path("id");
    }

    public String getClientId() {
        return data.path("clientId");
    }

}
