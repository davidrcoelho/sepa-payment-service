package eu.gentleart.sepa.payment.service.api;

import eu.gentleart.sepa.payment.service.AbstractApiTest;
import eu.gentleart.sepa.payment.service.helpers.OpenApiParser;
import eu.gentleart.sepa.payment.service.helpers.json.ChangeField;
import eu.gentleart.sepa.payment.service.helpers.json.DeleteField;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;

import static eu.gentleart.sepa.payment.service.PersistentController.HEADER_REQUEST_ID;
import static eu.gentleart.sepa.payment.service.api.AccountResourceTest.createAccount;
import static eu.gentleart.sepa.payment.service.api.MandateResourceTest.createMandate;
import static eu.gentleart.sepa.payment.service.api.MandateResourceTest.getMandate;
import static eu.gentleart.sepa.payment.service.helpers.UniqueNumberGenerator.newUUID;
import static org.hamcrest.Matchers.*;

public class PaymentResourceTest extends AbstractApiTest {

    @Test
    public void given_a_post_request_should_create_payments() {

        String requestId = newUUID();

        String clientAccountId = newUUID();
        String clientPaymentId = newUUID();

        String accountRequest = OpenApiParser.exampleOf("AccountRequest",
                new ChangeField("clientId", clientAccountId));

        int accountId = createAccount(accountRequest, requestId)
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");

        int mandateId = createMandate(accountId)
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");

        String paymentBatchRequest = OpenApiParser.exampleOf("PaymentBatchRequest",
                new ChangeField("$[0].clientAccountId", clientAccountId),
                new ChangeField("$[0].clientPaymentId", clientPaymentId));

        createPayment(paymentBatchRequest)
                .statusCode(HttpStatus.SC_OK);

        getMandate(accountId, mandateId)
                .statusCode(HttpStatus.SC_OK)
                .body("payments.size", equalTo(1))
                .body("payments[0].status", equalTo("CREATED"))
                .body("payments[0].clientPaymentId", equalTo(clientPaymentId));
    }

    @Test
    public void given_a_payment_request_without_creditorId_should_assign_default_creditor_to_the_payment() {
        String requestId = newUUID();

        String clientAccountId = newUUID();
        String clientPaymentId = newUUID();

        String accountRequest = OpenApiParser.exampleOf("AccountRequest",
                new ChangeField("clientId", clientAccountId));

        int accountId = createAccount(TENANT_NAME_FOR_TESTING2, accountRequest, requestId)
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");

        int mandateId = createMandate(TENANT_NAME_FOR_TESTING2, accountId)
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");

        String paymentBatchRequest = OpenApiParser.exampleOf("PaymentBatchRequest",
                new ChangeField("$[0].clientAccountId", clientAccountId),
                new ChangeField("$[0].clientPaymentId", clientPaymentId),
                new DeleteField("$[0].creditorId"));

        createPayment(TENANT_NAME_FOR_TESTING2, requestId, paymentBatchRequest)
                .statusCode(HttpStatus.SC_OK);

        getMandate(TENANT_NAME_FOR_TESTING2, accountId, mandateId)
                .statusCode(HttpStatus.SC_OK)
                .body("payments[0].creditor.id", equalTo(2))
                .body("payments[0].creditor.creditorKey", equalTo("creditorTenant2Test2"));
    }

    @Test
    public void given_an_account_with_two_mandates_should_add_payment_to_the_most_recent_mandate() {

        String requestId = newUUID();

        String clientAccountId = newUUID();
        String clientPaymentId = newUUID();

        String accountRequest = OpenApiParser.exampleOf("AccountRequest",
                new ChangeField("clientId", clientAccountId));

        int accountId = createAccount(accountRequest, requestId)
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");

        int firstMandateId = createMandate(accountId)
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");

        int secondMandateId = AmendmentResourceTest.createAmendment(accountId, firstMandateId)
                .statusCode(HttpStatus.SC_CREATED)
                .extract().path("id");

        String paymentBatchRequest = OpenApiParser.exampleOf("PaymentBatchRequest",
                new ChangeField("$[0].clientAccountId", clientAccountId),
                new ChangeField("$[0].clientPaymentId", clientPaymentId),
                new ChangeField("$[0].amount.value", 5.00));

        createPayment(paymentBatchRequest)
                .statusCode(HttpStatus.SC_OK);

        String paymentBatchRequest2 = OpenApiParser.exampleOf("PaymentBatchRequest",
                new ChangeField("$[0].clientAccountId", clientAccountId),
                new ChangeField("$[0].clientAccountId", clientAccountId),
                new ChangeField("$[0].amount.value", 10.00));

        createPayment(paymentBatchRequest2)
                .statusCode(HttpStatus.SC_OK);

        getMandate(accountId, firstMandateId)
                .statusCode(HttpStatus.SC_OK)
                .body("$", not(hasKey("payments")));

        getMandate(accountId, secondMandateId)
                .statusCode(HttpStatus.SC_OK)
                .body("payments.size", equalTo(2));
    }

    static ValidatableResponse createPayment(String requestBody) {
        return createPayment(newUUID(), requestBody);
    }

    static ValidatableResponse createPayment(String requestId, String mandateRequestBody) {
        return createPayment(TENANT_NAME_FOR_TESTING1, requestId, mandateRequestBody);
    }

    static ValidatableResponse createPayment(String requestUser, String requestId, String mandateRequestBody) {
        return
                given(requestUser)
                        .contentType(ContentType.JSON)
                        .header(HEADER_REQUEST_ID, requestId)
                .body(mandateRequestBody)
                        .post("/v1/batch-payments")
                        .then()
                        .log().all();
    }


}
