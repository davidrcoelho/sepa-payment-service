# sepa-payment-service

### Build and run
```bash
./gradlew clean build
java -jar build/libs/sepa-payment-service.jar
```

### Generate test metrics
```bash
./gradlew clean pitest
```

### Sonar
```bash
./gradlew clean build sonarqube -Dsonar.host.url=http://localhost:9000
```

### Local deployment
```bash

# Set Docker environment
eval $(minikube docker-env)

# Create config map
kubectl create configmap sepa-payment-service-apidoc --from-file=./src/testIntegration/resources/apidoc.yaml

# Build
./gradlew clean build

# Docker build
docker build -t local/sepa-payment-service:$(git rev-parse --short HEAD) .

# Helm install
helm upgrade --install --force sepa-payment-service ./kube \
    --namespace=default \
    --set spec.rules.host=cluster.local \
    --set image.application.name=local/sepa-payment-service:$(git rev-parse --short HEAD)


```

## Local environment

```
docker-compose up
```
