FROM openjdk:8-alpine

RUN mkdir /apps

COPY ./build/libs/sepa-payment-service.jar /apps/sepa-payment-service.jar

HEALTHCHECK --interval=10s --start-period=2m CMD curl -f http://localhost:8080/actuator/health | exit 1

CMD java -jar /apps/sepa-payment-service.jar

EXPOSE 8080
